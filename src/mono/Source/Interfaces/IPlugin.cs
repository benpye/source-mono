﻿namespace Source.Interfaces
{
	/// <summary>
	/// All classes implementing <see cref="IPlugin"/> will be instantiated
	/// and managed by PluginInterface.
	/// </summary>
	/// <example>
	/// To add a plugin all that needs to be done is to implement the <see cref="IPlugin"/> interface.
	/// The destructor will also be called when the plugin is being unloaded.
	/// <code>
	/// public class Test : IPlugin
	/// {
	/// 	public void Init()
	/// 	{
	/// 		Debug.Msg("TestPlugin loaded in domain: {0}\n", AppDomain.CurrentDomain.FriendlyName);
	/// 	}
	/// }
	/// </code>
	/// </example>
	public interface IPlugin
	{
		/// <summary>
		/// Called once all assemblies are loaded.
		/// </summary>
		void Init();
	}
}
