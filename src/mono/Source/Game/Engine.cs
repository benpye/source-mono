﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Source.Game
{
	public static class Engine
	{
		[DllImport("__Source")]
		private static extern bool engine_is_client();

		[DllImport("__Source")]
		private static extern float engine_real_time();

		[DllImport("__Source")]
		private static extern int engine_frame_count();

		[DllImport("__Source")]
		private static extern float engine_absolute_frame_time();

		[DllImport("__Source")]
		private static extern float engine_cur_time();

		[DllImport("__Source")]
		private static extern float engine_frame_time();

		[DllImport("__Source")]
		private static extern int engine_max_clients();

		[DllImport("__Source")]
		private static extern int engine_tick_count();

		[DllImport("__Source")]
		private static extern float engine_interval_per_tick();

		[DllImport("__Source")]
		private static extern float engine_interpolation_amount();

		[DllImport("__Source")]
		private static extern int engine_sim_ticks_this_frame();

		[DllImport("__Source")]
		private static extern int engine_network_protocol();

		public static bool IsClient
		{
			get { return engine_is_client(); }
		}

		public static float RealTime
		{
			get { return engine_real_time(); }
		}

		public static int FrameCount
		{
			get { return engine_frame_count(); }
		}

		public static float AbsoluteFrameTime
		{
			get { return engine_absolute_frame_time(); }
		}

		public static float CurTime
		{
			get { return engine_cur_time(); }
		}

		public static float FrameTime
		{
			get { return engine_frame_time(); }
		}

		public static int MaxClients
		{
			get { return engine_max_clients(); }
		}

		public static int TickCount
		{
			get { return engine_tick_count(); }
		}

		public static float IntervalPerTick
		{
			get { return engine_interval_per_tick(); }
		}

		public static float InterpolationAmount
		{
			get { return engine_interpolation_amount(); }
		}

		public static int SimTicksThisFrame
		{
			get { return engine_sim_ticks_this_frame(); }
		}

		public static int NetworkProtocol
		{
			get { return engine_network_protocol(); }
		}
	}
}
