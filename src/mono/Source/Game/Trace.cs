﻿using System;
using System.Runtime.InteropServices;

using Source.Public.Mathlib;
using Source.Public;

namespace Source.Game
{
	public class Trace
	{
		[DllImport("__Source")]
		private static extern /*trace_t **/ IntPtr trace_trace_line(/*Vector*/ IntPtr vecAbsStart, /*Vector*/ IntPtr vecAbsEnd, uint mask, IntPtr[] filterEnts, int entCount);

		[DllImport("__Source")]
		private static extern /*trace_t **/ IntPtr trace_trace_hull(/*Vector*/ IntPtr vecAbsStart, /*Vector*/ IntPtr vecAbsEnd, /*Vector*/ IntPtr hullMin, /*Vector*/ IntPtr hullMax, uint mask, IntPtr[] filterEnts, int entCount);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr trace_hit_normal(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr trace_end_position(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr trace_start_position(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool trace_start_solid(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool trace_all_solid(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern float trace_fraction_left_solid(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern float trace_fraction(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*CBaseEntity **/ IntPtr trace_hit_entity(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int trace_get_entity_index(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool trace_did_hit_non_world_entity(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool trace_did_hit_world(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool trace_did_hit(/*trace_t*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void trace_destroy_class(/*trace_t*/ IntPtr pObject);


		public Trace(Vector vecAbsStart, Vector vecAbsEnd, Mask mask = Mask.All, Entity[] filterEnts = null)
		{
			filterEnts = filterEnts ?? new Entity[] { };
			var entPtrs = new IntPtr[filterEnts.Length];
			for (var i = 0; i < filterEnts.Length; i++)
			{
				entPtrs[i] = filterEnts[i].NativePtr;
			}
			NativePtr = trace_trace_line(vecAbsStart.NativePtr, vecAbsEnd.NativePtr, (uint)mask, entPtrs, filterEnts.Length);
		}

		public Trace(Vector vecAbsStart, Vector vecAbsEnd, Vector hullMin, Vector hullMax, Mask mask = Mask.All, Entity[] filterEnts = null)
		{
			filterEnts = filterEnts ?? new Entity[] { };
			var entPtrs = new IntPtr[filterEnts.Length];
			for (var i = 0; i < filterEnts.Length; i++)
			{
				entPtrs[i] = filterEnts[i].NativePtr;
			}
			NativePtr = trace_trace_hull(vecAbsStart.NativePtr, vecAbsEnd.NativePtr, hullMin.NativePtr, hullMax.NativePtr, (uint)mask, entPtrs, filterEnts.Length);
		}

		~Trace()
		{
			trace_destroy_class(NativePtr);
		}

		public Vector HitNormal
		{
			get { return new Vector(trace_hit_normal(NativePtr)); }
		}

		public Vector EndPosition
		{
			get { return new Vector(trace_end_position(NativePtr)); }
		}

		public Vector StartPosition
		{
			get { return new Vector(trace_start_position(NativePtr)); }
		}

		public bool StartSolid
		{
			get { return trace_start_solid(NativePtr); }
		}

		public bool AllSolid
		{
			get { return trace_all_solid(NativePtr); }
		}

		public float FractionLeftSolid
		{
			get { return trace_fraction_left_solid(NativePtr); }
		}

		public float Fraction
		{
			get { return trace_fraction(NativePtr); }
		}

		public Entity HitEntity
		{
			get { return new Entity(trace_hit_entity(NativePtr)); }
		}

		public int EntityIndex
		{
			get { return trace_get_entity_index(NativePtr); }
		}

		public bool DidHitNonWorldEntity
		{
			get { return trace_did_hit_non_world_entity(NativePtr); }
		}

		public bool DidHitWorld
		{
			get { return trace_did_hit_world(NativePtr); }
		}

		public bool DidHit
		{
			get { return trace_did_hit(NativePtr); }
		}

		public IntPtr NativePtr;
	}
}
