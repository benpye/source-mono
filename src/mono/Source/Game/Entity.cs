﻿using System;
using System.Runtime.InteropServices;

using Source.Public.Mathlib;

namespace Source.Game
{
	public class Entity
	{
		[StructLayout(LayoutKind.Sequential)]
		private struct EntStruct
		{
			private readonly IntPtr ents;
			private readonly int count;

			public Entity[] ToEntityArray()
			{
				var retEnts = new Entity[count];
				var entPtrs = new IntPtr[count];
				Marshal.Copy(ents, entPtrs, 0, count);

				for (var i = 0; i < retEnts.Length; i++)
				{
					retEnts[i] = new Entity(entPtrs[i]);
				}

				entity_delete_struct(this);

				return retEnts;
			}
		}

		// Entity static functions
		[DllImport("__Source")]
		private static extern EntStruct entity_get_all();

		[DllImport("__Source")]
		private static extern EntStruct entity_get_in_sphere(int maxCount, /*Vector **/ IntPtr center, float radius, int flagMask);		
		
		[DllImport("__Source")]
		private static extern EntStruct entity_get_in_box(int maxCount, /*Vector **/ IntPtr mins, /*Vector **/ IntPtr maxs, int flagMask);

		[DllImport("__Source")]
		private static extern  IntPtr entity_get_by_index(int id);

		[DllImport("__Source")]
		private static extern IntPtr entity_create(string classname);

		[DllImport("__Source")]
		private static extern void entity_delete_struct(EntStruct entStruct);

		public static Entity[] GetAll()
		{
			var rawEnts = entity_get_all();
			return rawEnts.ToEntityArray();
		}

		public static Entity[] GetInSphere(int maxCount, Vector center, float radius, int flagMask)
		{
			var rawEnts = entity_get_in_sphere(maxCount, center.NativePtr, radius, flagMask);
			return rawEnts.ToEntityArray();
		}	
		
		public static Entity[] GetInBox(int maxCount, Vector mins, Vector maxs, int flagMask)
		{
			var rawEnts = entity_get_in_box(maxCount,  mins.NativePtr, maxs.NativePtr, flagMask);
			return rawEnts.ToEntityArray();
		}

		/// <summary>
		/// Returns a specified <see cref="Entity"/> by it's index.
		/// </summary>
		/// <param name="id">The index of the <see cref="Entity"/>.</param>
		/// <returns></returns>
		public static Entity GetByIndex(int id)
		{
			return new Entity(entity_get_by_index(id));
		}

		public static Entity Create(string classname)
		{
			return new Entity(entity_create(classname));
		}

		// Entity non-static functions
		[DllImport("__Source")]
		private static extern void baseentity_clear_effects(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_activate(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_spawn(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_ent_index(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_eflag_set(/*CBaseEntity*/ IntPtr pObject, int flags);

		[DllImport("__Source")]
		private static extern void baseentity_remove_eflags(/*CBaseEntity*/ IntPtr pObject, int flags);

		[DllImport("__Source")]
		private static extern void baseentity_add_eflags(/*CBaseEntity*/ IntPtr pObject, int flags);

		[DllImport("__Source")]
		private static extern void baseentity_set_eflags(/*CBaseEntity*/ IntPtr pObject, int flags);

		[DllImport("__Source")]
		private static extern int baseentity_get_eflags(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_health(/*CBaseEntity*/ IntPtr pObject, int health);

		[DllImport("__Source")]
		private static extern int baseentity_get_max_health(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_health(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_npc(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_weapon(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_in_world(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_world(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_player(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_gravity(/*CBaseEntity*/ IntPtr pObject, float gravity);

		[DllImport("__Source")]
		private static extern float baseentity_get_gravity(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_friction(/*CBaseEntity*/ IntPtr pObject, float friction);

		[DllImport("__Source")]
		private static extern float baseentity_emit_sound(/*CBaseEntity*/ IntPtr pObject, string soundname, float time);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_world_align_maxs(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_world_align_mins(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_left(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_forward(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_up(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_local_velocity(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr origin);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_get_local_velocity(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_abs_velocity(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr origin);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_get_abs_velocity(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_local_angles(/*CBaseEntity*/ IntPtr pObject, /*QAngle*/ IntPtr angles);

		[DllImport("__Source")]
		private static extern void baseentity_set_local_origin(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr origin);

		[DllImport("__Source")]
		private static extern /*QAngle **/ IntPtr baseentity_get_local_angles(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_get_local_origin(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_abs_angles(/*CBaseEntity*/ IntPtr pObject, /*QAngle*/ IntPtr angles);

		[DllImport("__Source")]
		private static extern void baseentity_set_abs_origin(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr origin);

		[DllImport("__Source")]
		private static extern /*QAngle **/ IntPtr baseentity_get_abs_angles(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_get_abs_origin(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_set_model(/*CBaseEntity*/ IntPtr pObject, string model);

		[DllImport("__Source")]
		private static extern /*char **/ IntPtr baseentity_get_model(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*char **/ IntPtr baseentity_get_debug_name(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*char **/ IntPtr baseentity_get_classname(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_world_space_center(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_vphysics_set_update(/*CBaseEntity*/ IntPtr pObject, /*IPhysicsObject*/ IntPtr physObj);

		[DllImport("__Source")]
		private static extern void baseentity_vphysics_set_object(/*CBaseEntity*/ IntPtr pObject, /*IPhysicsObject*/ IntPtr physObj);

		[DllImport("__Source")]
		private static extern /*IPhysicsObject **/ IntPtr baseentity_vphysics_init_static(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*IPhysicsObject **/ IntPtr baseentity_vphysics_init_shadow(/*CBaseEntity*/ IntPtr pObject, bool allowPhysicsMove, bool allowPhysicsRot, /*solid_t*/ IntPtr solid);

		[DllImport("__Source")]
		private static extern /*IPhysicsObject **/ IntPtr baseentity_vphysics_init_normal(/*CBaseEntity*/ IntPtr pObject, int solidType, int solidFlags, bool createAsleep, /*solid_t*/ IntPtr solid);

		[DllImport("__Source")]
		private static extern void baseentity_vphysics_destroy_object(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_unset_player_simulated(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_trace_bleed(/*CBaseEntity*/ IntPtr pObject, float damage, /*Vector*/ IntPtr dir, /*trace_t*/ IntPtr trace, int damageType);

		[DllImport("__Source")]
		private static extern void baseentity_toggle_flag(/*CBaseEntity*/ IntPtr pObject, int flag);

		[DllImport("__Source")]
		private static extern IntPtr baseentity_think_set(/*CBaseEntity*/ IntPtr pObject, IntPtr func, float nextThinkTime, string context);

		[DllImport("__Source")]
		private static extern bool baseentity_should_collide(/*CBaseEntity*/ IntPtr pObject, int collisionGroup, int contentsMask);

		[DllImport("__Source")]
		private static extern void baseentity_set_water_type(/*CBaseEntity*/ IntPtr pObject, int type);

		[DllImport("__Source")]
		private static extern void baseentity_set_view_offset(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr offset);

		[DllImport("__Source")]
		private static extern void baseentity_set_prediction_random_seed(/*CBaseEntity*/ IntPtr pObject, int seed);

		[DllImport("__Source")]
		private static extern void baseentity_set_player_simulated(/*CBaseEntity*/ IntPtr pObject, /*CBasePlayer*/ IntPtr player);

		[DllImport("__Source")]
		private static extern void baseentity_set_next_think(/*CBaseEntity*/ IntPtr pObject, float thinkTime, string context);

		[DllImport("__Source")]
		private static extern void baseentity_set_effects(/*CBaseEntity*/ IntPtr pObject, int effects);

		[DllImport("__Source")]
		private static extern void baseentity_set_effect_entity(/*CBaseEntity*/ IntPtr pObject, /*CBaseEntity*/ IntPtr effectEntity);

		[DllImport("__Source")]
		private static extern void baseentity_set_collision_group(/*CBaseEntity*/ IntPtr pObject, int group);

		[DllImport("__Source")]
		private static extern void baseentity_set_blocks_los(/*CBaseEntity*/ IntPtr pObject, bool blocksLOS);

		[DllImport("__Source")]
		private static extern void baseentity_set_allow_precache(/*CBaseEntity*/ IntPtr pObject, bool allow);

		[DllImport("__Source")]
		private static extern void baseentity_set_ai_walkable(/*CBaseEntity*/ IntPtr pObject, bool blocksLOS);

		[DllImport("__Source")]
		private static extern void baseentity_remove_flag(/*CBaseEntity*/ IntPtr pObject, int flagsToRemove);

		[DllImport("__Source")]
		private static extern int baseentity_register_think_context(/*CBaseEntity*/ IntPtr pObject, string context);

		[DllImport("__Source")]
		private static extern void baseentity_physics_touch_triggers(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr prevAbsOrigin);

		[DllImport("__Source")]
		private static extern void baseentity_make_trace(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr vecTracerSrc, /*trace_t*/ IntPtr trace, int tracerType);

		[DllImport("__Source")]
		private static extern /*QAngle **/ IntPtr baseentity_local_eye_angles(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_standable(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_simulating_on_alternate_ticks(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_precache_allowed(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_bsp_model(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_ai_walkable(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_impact_trace(/*CBaseEntity*/ IntPtr pObject, /*trace_t*/ IntPtr trace, int damageType, string impactName);

		[DllImport("__Source")]
		private static extern int baseentity_get_water_type(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_get_view_offset(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*char **/ IntPtr baseentity_get_tracer_type(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_tracer_attachment(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*CBaseEntity **/ IntPtr baseentity_get_root_move_parent(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_next_think_tick(/*CBaseEntity*/ IntPtr pObject, string context);

		[DllImport("__Source")]
		private static extern float baseentity_get_next_think(/*CBaseEntity*/ IntPtr pObject, string context);

		[DllImport("__Source")]
		private static extern int baseentity_get_last_think_tick(/*CBaseEntity*/ IntPtr pObject, string context);

		[DllImport("__Source")]
		private static extern float baseentity_get_last_think(/*CBaseEntity*/ IntPtr pObject, string context);

		[DllImport("__Source")]
		private static extern int baseentity_get_first_think_tick(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_follow_entity(/*CBaseEntity*/ IntPtr pObject, /*CBaseEntity*/ IntPtr other, bool boneMerge);

		[DllImport("__Source")]
		private static extern void baseentity_fire_bullets(/*CBaseEntity*/ IntPtr pObject, int nShots, /*Vector*/ IntPtr vecSrc, /*Vector*/ IntPtr vecDir, /*Vector*/ IntPtr vecSpread, float flDistance, int nAmmoType, bool bPrimaryAttack);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_eye_position(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*QAngle **/ IntPtr baseentity_eye_angles(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseentity_ear_position(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_do_impact_effect(/*CBaseEntity*/ IntPtr pObject, /*trace_t*/ IntPtr trace, int damageType);

		[DllImport("__Source")]
		private static extern void baseentity_dispatch_trace_attack(/*CBaseEntity*/ IntPtr pObject, /*CTakeDamageInfo*/ IntPtr dInfo, /*Vector*/ IntPtr dir, /*trace_t*/ IntPtr trace);

		[DllImport("__Source")]
		private static extern void baseentity_decal_trace(/*CBaseEntity*/ IntPtr pObject, /*trace_t*/ IntPtr trace, string name);

		[DllImport("__Source")]
		private static extern /*char **/ IntPtr baseentity_damage_decal(/*CBaseEntity*/ IntPtr pObject, int damageType, int material);

		[DllImport("__Source")]
		private static extern bool baseentity_create_vphysics(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_collision_rules_changed(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_clear_flags(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_blood_color(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_blocks_los(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseentity_apply_local_velocity_impulse(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr impulse);

		[DllImport("__Source")]
		private static extern void baseentity_apply_local_angular_velocity_impulse(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr impulse);

		[DllImport("__Source")]
		private static extern void baseentity_apply_abs_velocity_impulse(/*CBaseEntity*/ IntPtr pObject, /*Vector*/ IntPtr impulse);

		[DllImport("__Source")]
		private static extern void baseentity_add_flag(/*CBaseEntity*/ IntPtr pObject, int flag);

		[DllImport("__Source")]
		private static extern void baseentity_add_effects(/*CBaseEntity*/ IntPtr pObject, int effects);

		[DllImport("__Source")]
		private static extern void baseentity_set_simulation_time(/*CBaseEntity*/ IntPtr pObject, float st);

		[DllImport("__Source")]
		private static extern void baseentity_set_simulated_every_tick(/*CBaseEntity*/ IntPtr pObject, bool sim);

		[DllImport("__Source")]
		private static extern void baseentity_set_prediction_player(/*CBaseEntity*/ IntPtr pObject, /*CBasePlayer*/ IntPtr player);

		[DllImport("__Source")]
		private static extern void baseentity_set_animated_every_tick(/*CBaseEntity*/ IntPtr pObject, bool anim);

		[DllImport("__Source")]
		private static extern void baseentity_set_anim_time(/*CBaseEntity*/ IntPtr pObject, float time);

		[DllImport("__Source")]
		private static extern void baseentity_remove_effects(/*CBaseEntity*/ IntPtr pObject, int effects);

		[DllImport("__Source")]
		private static extern bool baseentity_is_simulated_every_tick(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_player_simulated(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_effect_active(/*CBaseEntity*/ IntPtr pObject, int effect);

		[DllImport("__Source")]
		private static extern bool baseentity_is_animated_every_tick(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern bool baseentity_is_alive(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern float baseentity_get_simulation_time(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*CBasePlayer **/ IntPtr baseentity_get_simulating_player(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_prediction_random_seed(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*CBasePlayer **/ IntPtr baseentity_get_prediction_player(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*CBaseEntity **/ IntPtr baseentity_get_owner_entity(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_move_type(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_move_collide(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_flags(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_effects(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*CBaseEntity **/ IntPtr baseentity_get_effect_entity(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseentity_get_collision_group(/*CBaseEntity*/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern float baseentity_get_anim_time(/*CBaseEntity*/ IntPtr pObject);

		public Entity(IntPtr nativePtr)
		{
			NativePtr = nativePtr;
		}
		
		public void ClearEffects()
		{
			baseentity_clear_effects(NativePtr);
		}
		public void Activate()
		{
			baseentity_activate(NativePtr);
		}
		public void Spawn()
		{
			baseentity_spawn(NativePtr);
		}

		public int EntIndex
		{
			get { return baseentity_get_ent_index(NativePtr); }
		}

		public bool IsEFlagSet(EFlag flags)
		{
			return baseentity_is_eflag_set(NativePtr, (int)flags);
		}
		public void RemoveEFlags(EFlag flags)
		{
			baseentity_remove_eflags(NativePtr, (int)flags);
		}
		public void AddEFlags(EFlag flags)
		{
			baseentity_add_eflags(NativePtr, (int)flags);
		}

		public EFlag EFlags
		{
			get { return (EFlag) baseentity_get_eflags(NativePtr); }
			set { baseentity_set_eflags(NativePtr, (int) value); }
		}

		public int Health
		{
			set { baseentity_set_health(NativePtr, value); }
			get { return baseentity_get_health(NativePtr); }
		}

		public int MaxHealth
		{
			get { return baseentity_get_max_health(NativePtr); }
		}

		public bool IsNPC
		{
			get { return baseentity_is_npc(NativePtr); }
		}

		public bool IsWeapon
		{
			get { return baseentity_is_weapon(NativePtr); }
		}

		public bool IsInWorld
		{
			get { return baseentity_is_in_world(NativePtr); }
		}

		public bool IsWorld
		{
			get { return baseentity_is_world(NativePtr); }
		}

		public bool IsPlayer
		{
			get { return baseentity_is_player(NativePtr); }
		}

		public float Gravity
		{
			set { baseentity_set_gravity(NativePtr, value); }
			get { return baseentity_get_gravity(NativePtr); }
		}

		public float Friction
		{
			set { baseentity_set_friction(NativePtr, value); }
		}

		public float EmitSound(string soundname, float time)
		{
			return baseentity_emit_sound(NativePtr, soundname, time);
		}

		public Vector WorldAlignMaxs
		{
			get { return new Vector(baseentity_world_align_maxs(NativePtr)); }
		}

		public Vector WorldAlignMins
		{
			get { return new Vector(baseentity_world_align_mins(NativePtr)); }
		}

		public Vector Left
		{
			get { return new Vector(baseentity_left(NativePtr)); }
		}

		public Vector Forward
		{
			get { return new Vector(baseentity_forward(NativePtr)); }
		}

		public Vector Up
		{
			get { return new Vector(baseentity_up(NativePtr)); }
		}

		public Vector LocalVelocity
		{
			set { baseentity_set_local_velocity(NativePtr, value.NativePtr); }
			get { return new Vector(baseentity_get_local_velocity(NativePtr)); }
		}

		public Vector AbsVelocity
		{
			set { baseentity_set_abs_velocity(NativePtr, value.NativePtr); }
			get { return new Vector(baseentity_get_abs_velocity(NativePtr)); }
		}

		public Angle LocalAngles
		{
			set { baseentity_set_local_angles(NativePtr, value.NativePtr); }
			get { return new Angle(baseentity_get_local_angles(NativePtr)); }
		}

		public Vector LocalOrigin
		{
			set { baseentity_set_local_origin(NativePtr, value.NativePtr); }
			get { return new Vector(baseentity_get_local_origin(NativePtr)); }
		}

		public Angle AbsAngles
		{
			set { baseentity_set_abs_angles(NativePtr, value.NativePtr); }
			get { return new Angle(baseentity_get_abs_angles(NativePtr)); }
		}

		public Vector AbsOrigin
		{
			set { baseentity_set_abs_origin(NativePtr, value.NativePtr); }
			get { return new Vector(baseentity_get_abs_origin(NativePtr)); }
		}

		public string Model
		{
			set { baseentity_set_model(NativePtr, value); }
			get { return Marshal.PtrToStringAnsi(baseentity_get_model(NativePtr)); }
		}

		public string DebugName
		{
			get { return Marshal.PtrToStringAnsi(baseentity_get_debug_name(NativePtr)); }
		}

		public string ClassName
		{
			get { return Marshal.PtrToStringAnsi(baseentity_get_classname(NativePtr)); }
		}

		public Vector WorldSpaceCenter
		{
			get { return new Vector(baseentity_world_space_center(NativePtr)); }
		}

		public void VPhysicsSetUpdate(/*IPhysicsObject*/ IntPtr physObj)
		{
			baseentity_vphysics_set_update(NativePtr, physObj);
		}
		public void VPhysicsSetObject(/*IPhysicsObject*/ IntPtr physObj)
		{
			baseentity_vphysics_set_object(NativePtr, physObj);
		}
		public /*IPhysicsObject **/ IntPtr VPhysicsInitStatic()
		{
			return baseentity_vphysics_init_static(NativePtr);
		}
		public /*IPhysicsObject **/ IntPtr VPhysicsInitShadow(bool allowPhysicsMove, bool allowPhysicsRot, /*solid_t*/ IntPtr solid)
		{
			return baseentity_vphysics_init_shadow(NativePtr, allowPhysicsMove, allowPhysicsRot, solid);
		}
		public /*IPhysicsObject **/ IntPtr VPhysicsInitNormal(int solidType, int solidFlags, bool createAsleep, /*solid_t*/ IntPtr solid)
		{
			return baseentity_vphysics_init_normal(NativePtr, solidType, solidFlags, createAsleep, solid);
		}
		public void VPhysicsDestroyObject()
		{
			baseentity_vphysics_destroy_object(NativePtr);
		}
		public void UnsetPlayerSimulated()
		{
			baseentity_unset_player_simulated(NativePtr);
		}
		public void TraceBleed(float damage, Vector dir, Trace trace, int damageType)
		{
			baseentity_trace_bleed(NativePtr, damage, dir.NativePtr, trace.NativePtr, damageType);
		}
		public void ToggleFlag(int flag)
		{
			baseentity_toggle_flag(NativePtr, flag);
		}
		public IntPtr ThinkSet(IntPtr func, float nextThinkTime, string context)
		{
			return baseentity_think_set(NativePtr, func, nextThinkTime, context);
		}
		public bool ShouldCollide(int collisionGroup, int contentsMask)
		{
			return baseentity_should_collide(NativePtr, collisionGroup, contentsMask);
		}

		public int WaterType
		{
			set { baseentity_set_water_type(NativePtr, value); }
			get { return baseentity_get_water_type(NativePtr); }
		}

		public Vector ViewOffset
		{
			set { baseentity_set_view_offset(NativePtr, value.NativePtr); }
			get { return new Vector(baseentity_get_view_offset(NativePtr)); }
		}

		public int PredictionRandomSeed
		{
			set { baseentity_set_prediction_random_seed(NativePtr, value); }
			get { return baseentity_get_prediction_random_seed(NativePtr); }
		}

		public Player PlayerSimulated
		{
			set { baseentity_set_player_simulated(NativePtr, value.NativePtr); }
		}

		public void SetNextThink(float thinkTime, string context)
		{
			baseentity_set_next_think(NativePtr, thinkTime, context);
		}

		public int Effects
		{
			set { baseentity_set_effects(NativePtr, value); }
			get { return baseentity_get_effects(NativePtr); }
		}

		public Entity EffectEntity
		{
			set { baseentity_set_effect_entity(NativePtr, value.NativePtr); }
			get { return new Entity(baseentity_get_effect_entity(NativePtr)); }
		}

		public int CollisionGroup
		{
			set { baseentity_set_collision_group(NativePtr, value); }
			get { return baseentity_get_collision_group(NativePtr); }
		}

		public bool BlocksLos
		{
			set { baseentity_set_blocks_los(NativePtr, value); }
		}

		public bool AllowPrecache
		{
			set { baseentity_set_allow_precache(NativePtr, value); }
		}

		public bool AIWalkable
		{
			set { baseentity_set_ai_walkable(NativePtr, value); }
			get { return baseentity_is_ai_walkable(NativePtr); }
		}

		public void RemoveFlag(int flagsToRemove)
		{
			baseentity_remove_flag(NativePtr, flagsToRemove);
		}
		public int RegisterThinkContext(string context)
		{
			return baseentity_register_think_context(NativePtr, context);
		}
		public void PhysicsTouchTriggers(Vector prevAbsOrigin)
		{
			baseentity_physics_touch_triggers(NativePtr, prevAbsOrigin.NativePtr);
		}
		public void MakeTrace(Vector vecTracerSrc, Trace trace, int tracerType)
		{
			baseentity_make_trace(NativePtr, vecTracerSrc.NativePtr, trace.NativePtr, tracerType);
		}

		public Angle LocalEyeAngles
		{
			get { return new Angle(baseentity_local_eye_angles(NativePtr)); }
		}

		public bool IsStandable
		{
			get { return baseentity_is_standable(NativePtr); }
		}

		public bool IsSimulatingOnAlternateTicks
		{
			get { return baseentity_is_simulating_on_alternate_ticks(NativePtr); }
		}

		public bool IsPrecacheAllowed
		{
			get { return baseentity_is_precache_allowed(NativePtr); }
		}

		public bool IsBSPModel
		{
			get { return baseentity_is_bsp_model(NativePtr); }
		}

		public void ImpactTrace(Trace trace, int damageType, string impactName)
		{
			baseentity_impact_trace(NativePtr, trace.NativePtr, damageType, impactName);
		}

		public string TracerType
		{
			get { return Marshal.PtrToStringAnsi(baseentity_get_tracer_type(NativePtr)); }
		}

		public int TracerAttachment
		{
			get { return baseentity_get_tracer_attachment(NativePtr); }
		}

		public Entity RootMoveParent
		{
			get { return new Entity(baseentity_get_root_move_parent(NativePtr)); }
		}

		public int GetNextThinkTick(string context)
		{
			return baseentity_get_next_think_tick(NativePtr, context);
		}
		public float GetNextThink(string context)
		{
			return baseentity_get_next_think(NativePtr, context);
		}
		public int GetLastThinkTick(string context)
		{
			return baseentity_get_last_think_tick(NativePtr, context);
		}
		public float GetLastThink(string context)
		{
			return baseentity_get_last_think(NativePtr, context);
		}

		public int FirstThinkTick
		{
			get { return baseentity_get_first_think_tick(NativePtr); }
		}

		public void FollowEntity(Entity other, bool boneMerge)
		{
			baseentity_follow_entity(NativePtr, other.NativePtr, boneMerge);
		}
		public void FireBullets(int nShots, Vector vecSrc, Vector vecDir, Vector vecSpread, float flDistance, int nAmmoType, bool bPrimaryAttack)
		{
			baseentity_fire_bullets(NativePtr, nShots, vecSrc.NativePtr, vecDir.NativePtr, vecSpread.NativePtr, flDistance, nAmmoType, bPrimaryAttack);
		}

		public Vector EyePosition
		{
			get { return new Vector(baseentity_eye_position(NativePtr)); }
		}

		public Angle EyeAngles
		{
			get { return new Angle(baseentity_eye_angles(NativePtr)); }
		}

		public Vector EarPosition
		{
			get { return new Vector(baseentity_ear_position(NativePtr)); }
		}

		public void DoImpactEffect(Trace trace, int damageType)
		{
			baseentity_do_impact_effect(NativePtr, trace.NativePtr, damageType);
		}
		public void DispatchTraceAttack(/*CTakeDamageInfo*/ IntPtr dInfo, Vector dir, Trace trace)
		{
			baseentity_dispatch_trace_attack(NativePtr, dInfo, dir.NativePtr, trace.NativePtr);
		}
		public void DecalTrace(Trace trace, string name)
		{
			baseentity_decal_trace(NativePtr, trace.NativePtr, name);
		}
		public string DamageDecal(int damageType, int material)
		{
			return Marshal.PtrToStringAnsi(baseentity_damage_decal(NativePtr, damageType, material));
		}
		public bool CreateVPhysics()
		{
			return baseentity_create_vphysics(NativePtr);
		}
		public void CollisionRulesChanged()
		{
			baseentity_collision_rules_changed(NativePtr);
		}
		public void ClearFlags()
		{
			baseentity_clear_flags(NativePtr);
		}

		public int BloodColor
		{
			get { return baseentity_blood_color(NativePtr); }
		}

		public bool BlocksLOS
		{
			get { return baseentity_blocks_los(NativePtr); }
		}

		public void ApplyLocalVelocityImpulse(Vector impulse)
		{
			baseentity_apply_local_velocity_impulse(NativePtr, impulse.NativePtr);
		}
		public void ApplyLocalAngularVelocityImpulse(Vector impulse)
		{
			baseentity_apply_local_angular_velocity_impulse(NativePtr, impulse.NativePtr);
		}
		public void ApplyAbsVelocityImpulse(Vector impulse)
		{
			baseentity_apply_abs_velocity_impulse(NativePtr, impulse.NativePtr);
		}
		public void AddFlag(int flag)
		{
			baseentity_add_flag(NativePtr, flag);
		}
		public void AddEffects(int effects)
		{
			baseentity_add_effects(NativePtr, effects);
		}

		public bool SimulatedEveryTick
		{
			set { baseentity_set_simulated_every_tick(NativePtr, value); }
		}

		public Player PredictionPlayer
		{
			set { baseentity_set_prediction_player(NativePtr, value.NativePtr); }
			get { return new Player(baseentity_get_prediction_player(NativePtr)); }
		}

		public bool AnimatedEveryTick
		{
			set { baseentity_set_animated_every_tick(NativePtr, value); }
		}

		public float AnimTime
		{
			set { baseentity_set_anim_time(NativePtr, value); }
			get { return baseentity_get_anim_time(NativePtr); }
		}

		public void RemoveEffects(int effects)
		{
			baseentity_remove_effects(NativePtr, effects);
		}

		public bool IsSimulatedEveryTick
		{
			get { return baseentity_is_simulated_every_tick(NativePtr); }
		}

		public bool IsPlayerSimulated
		{
			get { return baseentity_is_player_simulated(NativePtr); }
		}

		public bool IsEffectActive(int effect)
		{
			return baseentity_is_effect_active(NativePtr, effect);
		}

		public bool IsAnimatedEveryTick
		{
			get { return baseentity_is_animated_every_tick(NativePtr); }
		}

		public bool IsAlive
		{
			get { return baseentity_is_alive(NativePtr); }
		}

		public float SimulationTime
		{
			get { return baseentity_get_simulation_time(NativePtr); }
			set { baseentity_set_simulation_time(NativePtr, value); }
		}

		public Player SimulatingPlayer
		{
			get { return new Player(baseentity_get_simulating_player(NativePtr)); }
		}

		public Entity OwnerEntity
		{
			get { return new Entity(baseentity_get_owner_entity(NativePtr)); }
		}

		public int MoveType
		{
			get { return baseentity_get_move_type(NativePtr); }
		}

		public int MoveCollide
		{
			get { return baseentity_get_move_collide(NativePtr); }
		}

		public int Flags
		{
			get { return baseentity_get_flags(NativePtr); }
		}

		public IntPtr NativePtr;
	}
}
