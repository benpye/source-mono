﻿namespace Source.Game
{
	public enum EFlag
	{
		/// <summary>
		/// This entity is marked for death -- This allows the game to actually delete ents at a safe time
		/// </summary>
		KillMe	=							(1<<0),
		/// <summary>
		/// Entity is dormant, no updates to client
		/// </summary>
		Dormant	=							(1<<1),
		/// <summary>
		/// Lets us know when the noclip command is active.
		/// </summary>
		NoclipActive =						(1<<2),
		/// <summary>
		/// Set while a model is setting up its bones.
		/// </summary>
		SettingUpBones =					(1<<3),
		/// <summary>
		/// This is a special entity that should not be deleted when we restart entities only
		/// </summary>
		KeepOnRecreateEntities =			(1<<4),

		/// <summary>
		/// Client only- need shadow manager to update the shadow...
		/// </summary>
		DiryShadowUpdate =					(1<<5),
		/// <summary>
		/// Another entity is watching events on this entity (used by teleport)
		/// </summary>
		Notify =							(1<<6),

		/// <summary>
		/// The default behavior in ShouldTransmit is to not send an entity if it doesn't
		/// have a model. Certain entities want to be sent anyway because all the drawing logic
		/// is in the client DLL. They can set this flag and the engine will transmit them even
		/// if they don't have a model.
		/// </summary>
		ForceCheckTransmit =				(1<<7),

		/// <summary>
		/// This is set on bots that are frozen.
		/// </summary>
		BotFrozen =							(1<<8),
		/// <summary>
		/// Non-networked entity.
		/// </summary>
		ServerOnly =						(1<<9),
		/// <summary>
		/// Don't attach the edict; we're doing it explicitly
		/// </summary>
		NoAutoEdictAttach =					(1<<10),
	
		// Some dirty bits with respect to abs computations
		DirtyAbsTransform =					(1<<11),
		DirtyAbsVelocity =					(1<<12),
		DityAbsAndVelocity =				(1<<13),
		DirtySurroundingCollisionBounds	=	(1<<14),
		DirtySpatialPartition =				(1<<15),
		/// <summary>
		/// One of the child entities is a player.
		/// </summary>
		HasPlayerChild =					(1<<16),

		/// <summary>
		/// This is set if the entity detects that it's in the skybox.
		/// </summary>
		InSkybox =							(1<<17),
		/// <summary>
		/// Entities with this flag set show up in the partition even when not solid
		/// </summary>
		UsePartitionWhenNotSolid =			(1<<18),
		/// <summary>
		/// Used to determine if an entity is floating
		/// </summary>
		TouchingFluid =						(1<<19),

		// FIXME: Not really sure where I should add this...
		IsBeingLiftedByBarnacle =			(1<<20),
		/// <summary>
		/// I shouldn't be pushed by the rotorwash
		/// </summary>
		NoRotorWashPush =					(1<<21),
		NoThinkFunction =					(1<<22),
		NoGamePhysicsSimulation =			(1<<23),

		CheckUntouch =						(1<<24),
		/// <summary>
		/// I shouldn't block NPC line-of-sight
		/// </summary>
		DontBlockLOS =						(1 << 25),
		/// <summary>
		/// NPC's should not walk on this entity
		/// </summary>
		DontWalkOn =						(1 << 26),
		/// <summary>
		/// These guys shouldn't dissolve
		/// </summary>
		NoDissolve =						(1 << 27),
		/// <summary>
		/// Mega physcannon can't ragdoll these guys.
		/// </summary>
		NoMegaPhysCannonRagdoll =			(1<<28),
		/// <summary>
		/// Don't adjust this entity's velocity when transitioning into water
		/// </summary>
		NoWaterVelocityChange =				(1 << 29),
		/// <summary>
		/// Physcannon can't pick these up or punt them
		/// </summary>
		NoPhysCannonInteraction =			(1 << 30),
		/// <summary>
		/// Doesn't accept forces from physics damage
		/// </summary>
		NoDamageForces =					(1 << 31),
	}
}
