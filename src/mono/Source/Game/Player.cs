﻿using System;
using System.Collections.Generic;
using System.Management.Instrumentation;
using System.Runtime.InteropServices;

using Source.Public.Mathlib;

namespace Source.Game
{
	public enum HUDPrint
	{
		Notify = 1,
		Console = 2,
		Talk = 3,
		Center = 4,
	}

	[Serializable()]
	public class PlayerNotFoundException : System.Exception
	{
		public PlayerNotFoundException() : base() { }
		public PlayerNotFoundException(string message) : base(message) { }
		public PlayerNotFoundException(string message, System.Exception inner) : base(message, inner) { }

		// A constructor is needed for serialization when an 
		// exception propagates from a remoting server to the client.  
		protected PlayerNotFoundException(System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) { }
	}

	public class Player : Entity
	{
		[DllImport("__Source")]
		private static extern /*CBasePlayer **/ IntPtr player_get_local();

		[DllImport("__Source")]
		private static extern /*CBasePlayer **/ IntPtr player_get_by_index(int index);

		[DllImport("__Source")]
		private static extern /*CBasePlayer **/ IntPtr player_get_by_userid(int userID);

		public static Player Local
		{
			get { return new Player(player_get_local()); }
		}

		/// <summary>
		/// Returns a <see cref="Player"/> by it's entity index.
		/// </summary>
		/// <param name="index">The index of the player.</param>
		/// <returns>The <see cref="Player"/>.</returns>
		public new static Player GetByIndex(int index)
		{
			return new Player(player_get_by_index(index));
		}

		/// <summary>
		/// Returns a <see cref="Player"/> it's user id.
		/// </summary>
		/// <param name="userID">The user id of the player.</param>
		/// <returns>The <see cref="Player"/>.</returns>
		public static Player GetByUserID(int userID)
		{
			return new Player(player_get_by_userid(userID));
		}

		public static Player GetByName(string name)
		{
			for (var i = 0; i < Engine.MaxClients; i++)
			{
				var player = Player.GetByUserID(i);
				if(name == player.PlayerName)
					return player;
			}

			throw new PlayerNotFoundException();
		}

		public static Player[] GetAll()
		{
			List<Player> players = new List<Player>();

			for (var i = 0; i < Engine.MaxClients; i++)
			{
				var player = Player.GetByUserID(i);
				if(player.IsPlayer)
					players.Add(player);
			}

			return players.ToArray();
		}

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseplayer_get_player_mins(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*Vector **/ IntPtr baseplayer_get_player_maxs(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseplayer_select_item(/*CBasePlayer **/ IntPtr pObject, string weaponClass, int subType);

		[DllImport("__Source")]
		private static extern void baseplayer_view_punch(/*CBasePlayer **/ IntPtr pObject, /*QAngle **/ IntPtr angleOffset);

		[DllImport("__Source")]
		private static extern void baseplayer_view_punch_reset(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseplayer_get_default_fov(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern int baseplayer_get_fov(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern void baseplayer_set_fov(/*CBasePlayer **/ IntPtr pObject, float fov, int zoomRate);

		[DllImport("__Source")]
		private static extern void baseplayer_client_print(/*CBasePlayer **/ IntPtr pObject, int msgDest, string msg);

		[DllImport("__Source")]
		private static extern int baseplayer_get_user_id(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern UInt64 baseplayer_get_steam_id(/*CBasePlayer **/ IntPtr pObject);

		[DllImport("__Source")]
		private static extern /*char **/ IntPtr baseplayer_get_player_name(/*CBasePlayer **/ IntPtr pObject);

		public Player(IntPtr nativePtr) : base(nativePtr)
		{
		}

		public Player(Entity ent) : base(ent.NativePtr)
		{
		}

		public Vector PlayerMins
		{
			get { return new Vector(baseplayer_get_player_mins(NativePtr)); }
		}

		public Vector PlayerMaxs
		{
			get { return new Vector(baseplayer_get_player_maxs(NativePtr)); }
		}

		public void SelectItem(string weaponClass, int subType)
		{
			baseplayer_select_item(NativePtr, weaponClass, subType);
		}

		public void ViewPunch(Angle angleOffset)
		{
			baseplayer_view_punch(NativePtr, angleOffset.NativePtr);
		}

		public void ViewPunchReset()
		{
			baseplayer_view_punch_reset(NativePtr);
		}

		public int DefaultFOV
		{
			get { return baseplayer_get_default_fov(NativePtr); }
		}

		public int FOV
		{
			get { return baseplayer_get_fov(NativePtr); }
			set { baseplayer_set_fov(NativePtr, value, 0); }
		}

		public void SetFOV(float fov, int zoomRate)
		{
			baseplayer_set_fov(NativePtr, fov, zoomRate);
		}

		public void ClientPrint(HUDPrint msgDest, string msg)
		{
			baseplayer_client_print(NativePtr, (int)msgDest, msg);
		}

		public int UserID
		{
			get { return baseplayer_get_user_id(NativePtr); }
		}

		public ulong SteamID64
		{
			get { return baseplayer_get_steam_id(NativePtr); }
		}

		public string SteamID
		{
			get
			{
				var steamId = SteamID64;
				var accountID = steamId & 0xFFFFFFFF;
				return String.Format("STEAM_1:{0}:{1}", accountID & 0x1, accountID >> 1);
			}
		}

		public string PlayerName
		{
			get { return Marshal.PtrToStringAnsi(baseplayer_get_player_name(NativePtr)); }
		}
	}
}
