﻿using System.Collections.Generic;

using Source.Public;

namespace Source.Game
{
	/// <summary>
	/// Delegate used on handlers for game events.
	/// </summary>
	/// <param name="ev">The <see cref="EventData"/> of the calling event.</param>
	public delegate void GameEventDelegate(EventData ev);
	/// <summary>
	/// Provides a way for event handlers to be added and removed.
	/// </summary>
	public class Events
	{
		/// <summary>
		/// Event dictionary, used internally. Do not use.
		/// </summary>
		public static Dictionary<string, GameEventDelegate> EventHandlers = new Dictionary<string, GameEventDelegate>();

		/// <summary>
		/// Adds a handler for the event dictated by <paramref name="name"/>.
		/// </summary>
		/// <param name="name">The name of the event to handle.</param>
		/// <param name="del">The delegate to call.</param>
		public static void AddHandler(string name, GameEventDelegate del)
		{
			if (EventHandlers.ContainsKey(name))
				EventHandlers[name] += del;
			else
				EventHandlers.Add(name, del);
		}

		/// <summary>
		/// Removes a handler for the event dictated by <paramref name="name"/>.
		/// </summary>
		/// <param name="name">The name of the event.</param>
		/// <param name="del">The delegate to remove.</param>
		public static void RemoveHandler(string name, GameEventDelegate del)
		{
			EventHandlers[name] -= del;
		}
	}
}
