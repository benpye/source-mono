﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Json;

using Source.Public.Tier0;
using Source.Public;

namespace Source.Shared
{
	public class PluginInfo
	{
		public string Name { get; set; }
		public string Path { get; set; }
		public AppDomain Domain { get; set; }
		public PluginInterface Interface { get; set; }
	}

	public class AddonInfo
	{
		public AddonInfo()
		{
			Name = "N/A";
			Version = "N/A";
			LastUpdate = "N/A";
			Author = "N/A";
			Url = "N/A";
			Info = "N/A";
		}
		public string Name { get; set; }
		public string Version { get; set; }
		public string LastUpdate { get; set; }
		public string Author { get; set; }
		public string Url { get; set; }
		public string Info { get; set; }
		public string Directory { get; set; }
	}

	/// <summary>
	/// A class that manages the loading and calling of plugin dlls.
	/// </summary>
	public class PluginManager
	{
		private readonly List<PluginInfo> plugins;
		private readonly List<AddonInfo> addons;

		/// <summary>
		/// Initializes a new instance of the <see cref="PluginManager"/> class.
		/// </summary>
		public PluginManager()
		{
			plugins = new List<PluginInfo>();
			addons = new List<AddonInfo>();
		}

		public PluginInfo LoadPlugin(string path, string name)
		{
			var pluginDomain = AppDomain.CreateDomain(Guid.NewGuid().ToString());
			pluginDomain.UnhandledException += UnhandledExceptionHandler;
			var loader = (PluginInterface)pluginDomain.CreateInstanceAndUnwrap(
				Assembly.GetExecutingAssembly().FullName, typeof(PluginInterface).FullName);
			loader.Init(path);

			var info = new PluginInfo
			{
				Name = name,
				Path = path,
				Domain = pluginDomain,
				Interface = loader
			};

			return info;
		}

		/// <summary>
		/// Loads all plugins in the directory specified by <paramref name="directory"/>.
		/// </summary>
		/// <param name="directory">The directory to search for plugins.</param>
		public void LoadPlugins(string directory)
		{
			Debug.DevMsg("Searching {0} for plugins.\n", directory);
			var pluginPaths = Directory.GetFiles(directory, "*.dll");

			foreach (var pluginPath in pluginPaths)
			{
				Debug.DevMsg("Found plugin at: {0}\n", pluginPath);

				var info = LoadPlugin(pluginPath, pluginPath.Replace(DirectoryHelper.GetModDirectory(), ""));
				plugins.Add(info);
			}
		}

		/// <summary>
		/// Loads all plugins in the directory specified by <paramref name="directory"/>.
		/// </summary>
		/// <param name="directory">The directory to search for plugins.</param>
		public void LoadPlugins(string directory, string name)
		{
			Debug.DevMsg("Searching {0} for plugins.\n", directory);
			var pluginPaths = Directory.GetFiles(directory, "*.dll");

			foreach (var pluginPath in pluginPaths)
			{
				Debug.DevMsg("Found plugin at: {0}\n", pluginPath);

				var info = LoadPlugin(pluginPath, name);
				plugins.Add(info);
			}
		}

		public void LoadAddons(string host)
		{
			var addonsDir = DirectoryHelper.GetModDirectory() + "/addons";
			Debug.Msg("Addon dir {0}\n", addonsDir);
			var di = new DirectoryInfo(addonsDir);

			if (di.Exists)
			{
				foreach (var addonDir in di.EnumerateDirectories())
				{
					Debug.DevMsg("Addon directory {0}\n", addonDir.FullName);

					StreamReader addonInfo;

					try
					{
						addonInfo = File.OpenText(addonDir.FullName + "/addon.txt");
					}
					catch (FileNotFoundException e)
					{
						Debug.Warning("Addon in {0} missing addon.txt\n", addonDir.FullName);
						continue;
					}

					Debug.Msg("Opened addon.txt\n");

					JsonObject json;

					var rawFile = addonInfo.ReadToEnd();
					addonInfo.Close();

					try
					{
						json = JsonValue.Parse(rawFile) as JsonObject;
					}
					catch (Exception)
					{
						Debug.Warning("Invalid addon.txt in {0} (check your syntax at http://jsonlint.com/ )\n", 
							addonDir.FullName);
						continue;
					}

					if (!json.ContainsKey("name"))
					{
						Debug.Warning("addon.txt in {0} is missing name\n", addonDir.FullName);
						continue;
					}
					
					var addon = new AddonInfo();

					foreach (var key in json.Keys)
					{
						if (json[key].JsonType != JsonType.String)
							continue;

						switch (key)
						{
							case "author":
								addon.Author = json[key];
								break;
							case "info":
								addon.Info = json[key];
								break;
							case "lastupdate":
								addon.LastUpdate = json[key];
								break;
							case "name":
								addon.Name = json[key];
								break;
							case "url":
								addon.Url = json[key];
								break;
							case "version":
								addon.Version = json[key];
								break;
						}
					}

					addon.Directory = addonDir.FullName;

					addons.Add(addon);
					
					Filesystem.AddSearchPath(addon.Directory, "MOD");

					var addonPlugins = addonDir.FullName + "/plugins/" + host;
					if (new DirectoryInfo(addonPlugins).Exists)
					{
						LoadPlugins(addonPlugins, addon.Name);
					}
				}
			}
		}

		/// <summary>
		/// Unloads all plugins.
		/// </summary>
		public void UnloadPlugins()
		{
			foreach (var plugin in plugins)
			{
				AppDomain.Unload(plugin.Domain);
			}

			foreach (var addon in addons)
			{
				Filesystem.RemoveSearchPath(addon.Directory, "MOD");
			}

			plugins.Clear();
			addons.Clear();
		}

		/// <summary>
		/// Lists all currently loaded plugins to the console.
		/// </summary>
		public void ListPlugins()
		{
			Debug.Msg("Plugins\n");
			Debug.Msg("Plugin/Addon : Plugin Domain : Path\n");
			foreach (var plugin in plugins)
			{
				Debug.Msg("{0} : {1} : {2}\n", plugin.Name, plugin.Domain.FriendlyName, 
					plugin.Path.Replace(DirectoryHelper.GetModDirectory(), ""));
			}
		}

		/// <summary>
		/// Calls the game event specified by <paramref name="name"/> in all plugins.
		/// </summary>
		/// <param name="name">The name of the event to call.</param>
		/// <param name="ev">The <see cref="EventData"/>.</param>
		public void CallGameEvent(string name, EventData ev)
		{
			foreach (var plugin in plugins)
			{
				plugin.Interface.CallGameEvent(name, ev);
			}
		}

		public void ReloadPlugin(string path)
		{
			for (int i = 0; i < plugins.Count; i++)
			{
				var plugin = plugins[i];
				if (plugin.Path == path
					|| DirectoryHelper.GetModDirectory() + path == plugin.Path)
				{
					AppDomain.Unload(plugin.Domain);
					var info = LoadPlugin(plugin.Path, plugin.Name);
					plugins[i] = info;
					return;
				}
			}

			Debug.Msg("No plugin at {0}\n", path);
		}

		private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
		{
			var e = (Exception)args.ExceptionObject;
			Debug.Warning("Unhandled exception in plugin: {0}\n", e.Message);
		}
	}
}
