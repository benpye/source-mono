﻿using System;
using System.Reflection;
using System.IO;

namespace Source.Shared
{
	/// <summary>
	/// Provides helper functions to get directories in the mod folder.
	/// </summary>
	public static class DirectoryHelper
	{
		/// <summary>
		/// Calculates the mod/mono directory.
		/// </summary>
		/// <returns>The full path of the mod/mono directory.</returns>
		public static string GetMonoDirectory()
		{
			var asm = Assembly.GetExecutingAssembly();
			var path = asm.CodeBase;
			var uri = new UriBuilder(path);
			path = Uri.UnescapeDataString(uri.Path);
			path = Path.GetDirectoryName(path);

			var dirInfo = new DirectoryInfo(path);
			path = dirInfo.Parent.Parent.FullName;

			return path;
		}

		/// <summary>
		/// Calculates the mod directory.
		/// </summary>
		/// <returns>The full path of the mod directory.</returns>
		public static string GetModDirectory()
		{
			var asm = Assembly.GetExecutingAssembly();
			var path = asm.CodeBase;
			var uri = new UriBuilder(path);
			path = Uri.UnescapeDataString(uri.Path);
			path = Path.GetDirectoryName(path);

			var dirInfo = new DirectoryInfo(path);
			path = dirInfo.Parent.Parent.Parent.FullName;

			return path;
		}
	}
}
