﻿using System.Text;
using System.IO;

using Source.Public.Tier0;

namespace Source.Shared
{
	public class ConsoleWrapper : TextWriter
	{
		public override Encoding Encoding
		{
			get { return Encoding.UTF8; }
		}

		public override void Write(char value)
		{
			Debug.Msg(value.ToString());
		}
	}
}
