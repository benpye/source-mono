﻿namespace Source.Shared
{
	/// <summary>
	/// Shared content for both server and client side operations. Not bindings for any functionality but instead
	/// manage the plugin system amongst other things.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{
	}
}
