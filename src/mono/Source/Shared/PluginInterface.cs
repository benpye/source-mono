﻿using System;
using System.Linq;
using System.Reflection;
using Source.Game;
using Source.Interfaces;
using Source.Public;
using Source.Public.Tier0;

namespace Source.Shared
{
	/// <summary>
	/// The cross <see cref="AppDomain"/> interface for plugin loading and hooking.
	/// </summary>
	public class PluginInterface : MarshalByRefObject
	{
		private static IPlugin[] plugins;

		/// <summary>
		/// Loads the plugin specified by <paramref name="name"/> and calls it's Init function.
		/// </summary>
		/// <param name="name">The full path of the plugin to load.</param>
		public void Init(string name)
		{
			Console.SetOut(new ConsoleWrapper());

			var pluginAssembly = Assembly.LoadFrom(name);

			plugins =	(from t in pluginAssembly.GetTypes()
						where t.GetInterfaces().Contains(typeof(IPlugin))
						&& t.GetConstructor(Type.EmptyTypes) != null
						 select Activator.CreateInstance(t) as IPlugin).ToArray();

			foreach (var plugin in plugins)
			{
				plugin.Init();
			}
		}

		/// <summary>
		/// Calls the game event specified by <paramref name="name"/>.
		/// </summary>
		/// <param name="name">The name of the event to call.</param>
		/// <param name="ev">The <see cref="EventData"/>.</param>
		public void CallGameEvent(string name, EventData ev)
		{
			GameEventDelegate del;
			Events.EventHandlers.TryGetValue(name, out del);

			if (del != null)
			{
				try
				{
					del.Invoke(ev);
				}
				catch (Exception e)
				{
					Debug.Warning(String.Format("Event handler for {0} in {1} threw exception: {2}\n", name, AppDomain.CurrentDomain.FriendlyName, e.Message));
				}
			}
		}
	}
}
