﻿using System;
using System.Runtime.InteropServices;

namespace Source.Public
{
	/// <summary>
	/// Provides bindings to the game event system in Source. Allows for events to be fired, set and queried.
	/// </summary>
    public class EventData : MarshalByRefObject
    {
		[DllImport("__Source")]
		private static extern IntPtr gameevent_create_event(string name);

		[DllImport("__Source")]
		private static extern void gameevent_destroy_event(IntPtr gameevent);

		[DllImport("__Source")]
		private static extern IntPtr gameevent_duplicate_event(IntPtr gameevent);

		[DllImport("__Source")]
		private static extern void gameevent_fire(IntPtr gameevent, bool serveronly);

		[DllImport("__Source")]
		private static extern void gameevent_fire_clientside(IntPtr gameevent);

		[DllImport("__Source")]
		private static extern IntPtr gameevent_get_name(IntPtr gameevent);

		[DllImport("__Source")]
		private static extern bool gameevent_is_reliable(IntPtr gameevent);

		[DllImport("__Source")]
		private static extern bool gameevent_is_local(IntPtr gameevent);

		[DllImport("__Source")]
		private static extern bool gameevent_is_empty(IntPtr gameevent, string keyname);

		[DllImport("__Source")]
		private static extern bool gameevent_get_bool(IntPtr gameevent, string keyname, bool defaultvalue);

		[DllImport("__Source")]
		private static extern int gameevent_get_int(IntPtr gameevent, string keyname, int defaultvalue);

		[DllImport("__Source")]
		private static extern UInt64 gameevent_get_uint64(IntPtr gameevent, string keyname, UInt64 defaultvalue);

		[DllImport("__Source")]
		private static extern float gameevent_get_float(IntPtr gameevent, string keyname, float defaultvalue);

		[DllImport("__Source")]
		private static extern IntPtr gameevent_get_string(IntPtr gameevent, string keyname, string defaultvalue);

		[DllImport("__Source")]
		private static extern void gameevent_set_bool(IntPtr gameevent, string keyname, bool val);

		[DllImport("__Source")]
		private static extern void gameevent_set_int(IntPtr gameevent, string keyname, int val);

		[DllImport("__Source")]
		private static extern void gameevent_set_uint64(IntPtr gameevent, string keyname, UInt64 val);

		[DllImport("__Source")]
		private static extern void gameevent_set_float(IntPtr gameevent, string keyname, float val);

		[DllImport("__Source")]
		private static extern void gameevent_set_string(IntPtr gameevent, string keyname, string val);

		/// <summary>
		/// Initializes a new instance of the <see cref="EventData"/> class for the event specified by <paramref name="name"/>.
		/// </summary>
		/// <param name="name">The name of the event to fire.</param>
		public EventData(string name)
		{
			NativePtr = gameevent_create_event(name);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EventData"/> class. Used internally, do not use.
		/// </summary>
		/// <param name="nativePtr">The native pointer for the class instance.</param>
		public EventData(IntPtr nativePtr)
		{
			NativePtr = nativePtr;
		}

		/// <summary>
		/// Finalizes an instance of the <see cref="EventData"/> class.
		/// </summary>
		~EventData()
		{
			gameevent_destroy_event(NativePtr);
		}

		/// <summary>
		/// Fires the event.
		/// </summary>
		/// <param name="serveronly">if set to <c>true</c> the event will only fire serverside.</param>
		public void Fire(bool serveronly = false)
		{
			gameevent_fire(NativePtr, serveronly);
		}

		/// <summary>
		/// Fires the event clientside, faking a networked event.
		/// </summary>
		public void FireClientside()
		{
			gameevent_fire_clientside(NativePtr);
		}

		/// <summary>
		/// Gets the name of the event.
		/// </summary>
		/// <value> The name. </value>
		public string Name
		{
			get { return Marshal.PtrToStringAnsi(gameevent_get_name(NativePtr)); }
		}

		/// <summary>
		/// Determines whether this instance is networked reliably.
		/// </summary>
		/// <value>
		///		<c>true</c> if this instance is reliably networked; otherwise, <c>false</c>.
		/// </value>
		public bool Reliable
		{
			get { return gameevent_is_reliable(NativePtr); }
		}

		/// <summary>
		/// Determines whether this instance is never networked.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is never networked; otherwise, <c>false</c>.
		/// </value>
		public bool Local
		{
			get { return gameevent_is_local(NativePtr); }
		}

		/// <summary>
		/// Determines whether the specified key exists.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <returns>
		///   <c>false</c> if the specified key exists; otherwise, <c>false</c>.
		/// </returns>
		public bool IsEmpty(string keyname = "")
		{
			return gameevent_is_empty(NativePtr, keyname);
		}

		/// <summary>
		/// Gets the value associated with the specified key.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="defaultValue">Value to return if the key doesn't exist.</param>
		/// <returns>The value of the key.</returns>
		public T Get<T>(string keyname, T defaultValue = default(T))
		{
			var type = typeof (T);

			if (type == typeof(bool))
			{
				return (T)(object) GetBool(keyname, (bool)(object) defaultValue);
			}

			if (type == typeof(int))
			{
				return (T)(object) GetInt(keyname, (int)(object) defaultValue);
			}

			if(type == typeof(UInt64))
			{
				return (T)(object) GetUint64(keyname, (UInt64)(object) defaultValue);
			}
			
			if(type == typeof(float))
			{
				return (T)(object) GetFloat(keyname, (float)(object) defaultValue);
			}

			if (type == typeof(string))
			{
				return (T)(object) GetString(keyname, (string)(object) defaultValue);
			}

			return defaultValue;
		}

		/// <summary>
		/// Sets the value of the keyvalue pair to the specified value.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="value">The value for the keyvalue to be set to.</param>
		public void Set<T>(string keyname, T value)
		{
			var type = typeof(T);

			if (type == typeof(bool))
			{
				SetBool(keyname, (bool)(object) value);
			}

			if (type == typeof(int))
			{
				SetInt(keyname, (int) (object) value);
			}

			if (type == typeof(UInt64))
			{
				SetUint64(keyname, (UInt64) (object) value);
			}

			if (type == typeof(float))
			{
				SetFloat(keyname, (float) (object) value);
			}

			if (type == typeof(string))
			{
				SetString(keyname, (string) (object) value);
			}
		}

		/// <summary>
		/// Gets the bool associated with the specified key.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="defaultvalue">Value to return if the key doesn't exist.</param>
		/// <returns>The value of the related bool.</returns>
		public bool GetBool(string keyname, bool defaultvalue = false)
		{
			return gameevent_get_bool(NativePtr, keyname, defaultvalue);
		}

		/// <summary>
		/// Gets the int associated with the specified key.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="defaultvalue">Value to return if the key doesn't exist.</param>
		/// <returns>The value of the related int.</returns>
		public int GetInt(string keyname, int defaultvalue = 0)
		{
			return gameevent_get_int(NativePtr, keyname, defaultvalue);
		}

		/// <summary>
		/// Gets the UInt64 associated with the specified key.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="defaultvalue">Value to return if the key doesn't exist.</param>
		/// <returns>The value of the related UInt64.</returns>
		public UInt64 GetUint64(string keyname, UInt64 defaultvalue = 0)
		{
			return gameevent_get_uint64(NativePtr, keyname, defaultvalue);
		}

		/// <summary>
		/// Gets the float associated with the specified key.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="defaultvalue">Value to return if the key doesn't exist.</param>
		/// <returns>The value of the related float.</returns>
		public float GetFloat(string keyname, float defaultvalue = 0.0f)
		{
			return gameevent_get_float(NativePtr, keyname, defaultvalue);
		}

		/// <summary>
		/// Gets the string associated with the specified key.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="defaultvalue">Value to return if the key doesn't exist.</param>
		/// <returns>The value of the related string.</returns>
		public string GetString(string keyname, string defaultvalue = "")
		{
			return Marshal.PtrToStringAnsi(gameevent_get_string(NativePtr, keyname, defaultvalue));
		}

		/// <summary>
		/// Sets the value of the keyvalue pair to a bool.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="val">The value for the keyvalue to be set to.</param>
		public void SetBool(string keyname, bool val)
		{
			gameevent_set_bool(NativePtr, keyname, val);
		}

		/// <summary>
		/// Sets the value of the keyvalue pair to an int.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="val">The value for the keyvalue to be set to.</param>
		public void SetInt(string keyname, int val)
		{
			gameevent_set_int(NativePtr, keyname, val);
		}

		/// <summary>
		/// Sets the value of the keyvalue pair to a UInt64.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="val">The value for the keyvalue to be set to.</param>
		public void SetUint64(string keyname, UInt64 val)
		{
			gameevent_set_uint64(NativePtr, keyname, val);
		}

		/// <summary>
		/// Sets the value of the keyvalue pair to a float.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="val">The value for the keyvalue to be set to.</param>
		public void SetFloat(string keyname, float val)
		{
			gameevent_set_float(NativePtr, keyname, val);
		}

		/// <summary>
		/// Sets the value of the keyvalue pair to a string.
		/// </summary>
		/// <param name="keyname">The key.</param>
		/// <param name="val">The value for the keyvalue to be set to.</param>
		public void SetString(string keyname, string val)
		{
			gameevent_set_string(NativePtr, keyname, val);
		}

		/// <summary>
		/// The native pointer associated with this instance.
		/// </summary>
		public IntPtr NativePtr;
    }
}
