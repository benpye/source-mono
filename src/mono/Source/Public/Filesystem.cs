﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Source.Public
{
	public static class Filesystem
	{
		[DllImport("__Source")]
		private static extern void filesystem_add_search_path(string path, string pathID);

		[DllImport("__Source")]
		private static extern void filesystem_remove_search_path(string path, string pathID);

		public static void AddSearchPath(string path, string pathID)
		{
			filesystem_add_search_path(path, pathID);
		}

		public static void RemoveSearchPath(string path, string pathID)
		{
			filesystem_remove_search_path(path, pathID);
		}
	}
}
