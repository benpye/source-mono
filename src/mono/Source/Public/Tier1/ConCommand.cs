﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Source.Public.Tier0;

namespace Source.Public.Tier1
{
	[StructLayout(LayoutKind.Sequential)]
	struct ArgStruct
	{
		public IntPtr strings;
		public int count;
	}
	
	/// <summary>
	/// The delegate used when providing a function for a console command.
	/// </summary>
	/// <param name="args">An array of the arguments including the command name.</param>
	public delegate void ConCommandHandler(string[] args);

	public class ConCommandBase : MarshalByRefObject
	{
		[DllImport("__Source")]
		private static extern bool concommandbase_is_command(IntPtr command);

		[DllImport("__Source")]
		private static extern void concommandbase_add_flags(IntPtr command, int flags);

		[DllImport("__Source")]
		private static extern void concommandbase_remove_flags(IntPtr command, int flags);

		[DllImport("__Source")]
		private static extern int concommandbase_get_flags(IntPtr command);

		[DllImport("__Source")]
		private static extern IntPtr concommandbase_get_name(IntPtr command);

		[DllImport("__Source")]
		private static extern IntPtr concommandbase_get_help(IntPtr command);

		protected IntPtr nativePtr;

		public bool IsCommand
		{
			get { return concommandbase_is_command(nativePtr); }
		}

		public void AddFlags(FCVAR flags)
		{
			concommandbase_add_flags(nativePtr, (int)flags);
		}

		public void RemoveFlags(FCVAR flags)
		{
			concommandbase_remove_flags(nativePtr, (int)flags);
		}

		public FCVAR Flags
		{
			get { return (FCVAR)concommandbase_get_flags(nativePtr); }
		}

		public string Name
		{
			get { return Marshal.PtrToStringAnsi(concommandbase_get_name(nativePtr)); }
		}

		public string HelpText
		{
			get { return Marshal.PtrToStringAnsi(concommandbase_get_help(nativePtr)); }
		}
	}

	/// <summary>
	/// Allows for console commands to be created in managed code.
	/// </summary>
	/// <example>
	/// This allows for a console command to be easily added as a <c>static</c> variable in a class.
	/// Commands defined will be automatically removed when the object is destroyed, so
	///  <c>static</c> instances of the <see cref="ConCommand"/> class will exist so long as the plugin does.
	/// <code>
	/// static ConCommand HelloWorld = new ConCommand("hello_world", new ConCommandHandler(HelloWorldHandler));
	/// 
	/// private static void HelloWorldHandler(string[] args)
	/// {
	/// 	Debug.Msg("Hello World!\n");
	/// }
	/// </code>
	/// </example>
	public class ConCommand : ConCommandBase
	{
		[DllImport("__Source")]
		private static extern IntPtr add_concmd(string command, string help, int flags, InternalConCmdHandler handler);

		[DllImport("__Source")]
		private static extern void remove_concmd(string command);
		private delegate void InternalConCmdHandler(ArgStruct args);

		private static InternalConCmdHandler cmdHandler;
		private static Dictionary<string, ConCommandHandler> commandHandlers;
		private static bool isInited;

		private readonly string commandName;

		/// <summary>
		/// Initializes a new instance of the <see cref="ConCommand" /> class.
		/// </summary>
		/// <param name="command">The command name.</param>
		/// <param name="handler">The handler to be run when the command is executed.</param>
		/// <param name="help">The help string.</param>
		/// <param name="flags">The flags for the command.</param>
		public ConCommand(string command, ConCommandHandler handler, string help = "", FCVAR flags = FCVAR.None)
		{
			if (!isInited)
				Init();

			nativePtr = Add(command, help, handler, flags);
			commandName = command;
		}

		/// <summary>
		/// Finalizes an instance of the <see cref="ConCommand"/> class.
		/// </summary>
		~ConCommand()
		{
			Remove(commandName);
		}

		/// <summary>
		/// Internal function - will break stuff - do not use.
		/// </summary>
		public static void Init()
		{
			cmdHandler = CommandHandler;
			commandHandlers = new Dictionary<string, ConCommandHandler>();
			isInited = true;
		}

		private static IntPtr Add(string command, string help, ConCommandHandler handler, FCVAR flags)
		{
			if (commandHandlers.ContainsKey(command))
			{
				Debug.DevWarning("Attempting to re-register command {0}\n", command);
				throw new Exception();
			}

			var concommand = add_concmd(command, help, (int)flags, CommandHandler);
			commandHandlers.Add(command, handler);
			return concommand;
		}

		private static void Remove(string command)
		{
			if (commandHandlers.Remove(command))
				remove_concmd(command);
			else
				Debug.DevWarning("Attempting to remove non existant command {0}\n", command);
		}

		private static void CommandHandler(ArgStruct args)
		{
			var strings = new string[args.count];
			var stringPtr = new IntPtr[args.count];
			Marshal.Copy(args.strings, stringPtr, 0, args.count);

			for (int i = 0; i < args.count; i++)
			{
				strings[i] = Marshal.PtrToStringAnsi(stringPtr[i]);
			}

			ConCommandHandler callback;
			if (commandHandlers.TryGetValue(strings[0], out callback))
			{
				try
				{
					callback.Invoke(strings);
				}
				catch (Exception e)
				{
					Debug.Warning(String.Format("Connsole command for {0} in {1} threw exception: {2}\n", strings[0], AppDomain.CurrentDomain.FriendlyName, e.Message));
				}
			}
			else
				Debug.DevWarning("Unregistered command called {0}\n", strings[0]);
		}
    }
}
