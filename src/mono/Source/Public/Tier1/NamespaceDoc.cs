﻿namespace Source.Public.Tier1
{
	/// <summary>
	/// Binding for the Tier1 library, used for higher level functions such as CVars and ConCommands.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{
	}
}
