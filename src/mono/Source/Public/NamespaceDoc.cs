﻿namespace Source.Public
{
	/// <summary>
	/// Bindings for functions and classes that exist outside of any specific library, but are not game specific.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{
	}
}
