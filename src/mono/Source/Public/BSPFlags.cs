﻿namespace Source.Public
{
	public enum Contents
	{
		/// <summary>
		/// No contents
		/// </summary>
		Empty = 0,

		/// <summary>
		/// An eye is never valid in a solid.
		/// </summary>
		Solid = 0x1,
		/// <summary>
		/// Translucent, but not watery, eg. glass.
		/// </summary>
		Window = 0x2,
		Aux = 0x4,
		/// <summary>
		/// Alpha tested "grate" textures. Bullets/sight pass through, solids don't.
		/// </summary>
		Grate = 0x8,
		Slime = 0x10,
		Water = 0x20,
		/// <summary>
		/// Block AI line of sight.
		/// </summary>
		BlockLOS = 0x40,
		/// <summary>
		/// Things that cannot be seen through, may be non-solid.
		/// </summary>
		Opaque = 0x80,
		LastVisible = Opaque,

		AllVisible = (LastVisible | (LastVisible-1)),
	
		TestFogVolume = 0x100,
		Unused = 0x200,	

		// unused 
		// NOTE: If it's visible, grab from the top + update LAST_VISIBLE_CONTENTS
		// if not visible, then grab from the bottom.
		// OPAQUE + NODRAW count as OPAQUE (shadow-casting toolsblocklight textures)
		/// <summary>
		/// Unused.
		/// </summary>
		BlockLight = 0x400,

		/// <summary>
		/// Per team contents used to differentiate collisions between players and objects on different teams.
		/// </summary>
		Team1 = 0x800,
		/// <summary>
		/// Per team contents used to differentiate collisions between players and objects on different teams.
		/// </summary>
		Team2 = 0x1000,

		/// <summary>
		/// Igore opaque on surfaces that have nodraw.
		/// </summary>
		IgnoreNodrawOpaque = 0x2000,

		/// <summary>
		/// Hits entities which are MOVETYPE_PUSH (doors, plats, etc.)
		/// </summary>
		Moveable = 0x4000,

		// remaining contents are non-visible, and don't eat brushes
		AreaPortal = 0x8000,

		PlayerClip = 0x10000,
		MonsterClip = 0x20000,

		// currents can be added to any other contents, and may be mixed
		Current0 = 0x40000,
		Current90 = 0x80000,
		Current180 = 0x100000,
		Current270 = 0x200000,
		CurrentUp = 0x400000,
		CurrentDown = 0x800000,

		/// <summary>
		/// Removed before bsping an entity.
		/// </summary>
		Origin = 0x1000000,

		/// <summary>
		/// Should never be on a brush, only in game.
		/// </summary>
		Monster = 0x2000000,
		Debris = 0x4000000,
		/// <summary>
		/// Brushes to be added after vis leafs
		/// </summary>
		Detail = 0x8000000,
		/// <summary>
		/// Auto set if any surface has trans
		/// </summary>
		Translucent = 0x10000000,
		Ladder = 0x20000000,

		/// <summary>
		/// Use accurate hitboxes on trace
		/// </summary>
		Hitbox = 0x40000000,
	}

	public enum Surf
	{
		/// <summary>
		/// Value will hold the light strength.
		/// </summary>
		Light = 0x0001,
		/// <summary>
		/// Don't draw, indicates we should skylight + draw 2d sky, but not draw the 3d skybox.
		/// </summary>
		Sky2D = 0x0002,
		/// <summary>
		/// Don't draw, but add to skybox.
		/// </summary>
		Sky = 0x0004,
		/// <summary>
		/// Turbulent water warp.
		/// </summary>
		Warp = 0x0008,
		Trans = 0x0010,
		/// <summary>
		/// The surface cannot have a portal placed on it.
		/// </summary>
		NoPortal = 0x0020,
		/// <summary>
		/// This is an xbox hack to work around elimination of trigger surfaces, which break occluders.
		/// </summary>
		Trigger = 0x0040,
		/// <summary>
		/// Don't bother referencing the texture.
		/// </summary>
		Nodraw = 0x0080,

		/// <summary>
		/// Make a primary BSP splitter.
		/// </summary>
		Hint = 0x0100,

		/// <summary>
		/// Completely ignore, allowing non-closed brushes.
		/// </summary>
		Skip = 0x0200,
		/// <summary>
		/// Don't calculate light.
		/// </summary>
		NoLight = 0x0400,
		/// <summary>
		/// Calculate three lightmaps for the surface for bumpmapping.
		/// </summary>
		BumpLight = 0x0800,
		/// <summary>
		/// Don't recieve shadows.
		/// </summary>
		NoShadows = 0x1000,
		/// <summary>
		/// Don't receive decals.
		/// </summary>
		NoDecals = 0x2000,
		/// <summary>
		/// The surface cannot have paint placed on it.
		/// </summary>
		NoPaint = NoDecals,
		/// <summary>
		/// Don't subdivide patches on this surface.
		/// </summary>
		NoChop = 0x4000,
		/// <summary>
		/// Surface is part of a hitbox.
		/// </summary>
		Hitbox = 0x8000,
	}

	public enum Mask : uint
	{
		/// <summary>
		/// Everything.
		/// </summary>
		All = (0xFFFFFFFF),

		/// <summary>
		/// Everything that is normally solid.
		/// </summary>
		Solid = (Contents.Solid|Contents.Moveable|Contents.Window|Contents.Monster|Contents.Grate),

		/// <summary>
		/// Everything that blocks player movement.
		/// </summary>
		PlayerSolid = (Contents.Solid|Contents.Moveable|Contents.PlayerClip|Contents.Window|Contents.Monster|Contents.Grate),
		
		/// <summary>
		/// Blocks NPC movement.
		/// </summary>
		NPCSolid = (Contents.Solid|Contents.Moveable|Contents.MonsterClip|Contents.Window|Contents.Monster|Contents.Grate),
		
		/// <summary>
		/// Blocks fluid movement.
		/// </summary>
		NPCFluid = (Contents.Solid|Contents.Moveable|Contents.MonsterClip|Contents.Window|Contents.Monster),
		
		/// <summary>
		/// Water physics in these contents.
		/// </summary>
		Water = (Contents.Water|Contents.Moveable|Contents.Slime),
		
		/// <summary>
		/// Everything that blocks lighting.
		/// </summary>
		Opaque = (Contents.Solid|Contents.Moveable|Contents.Opaque),
		
		/// <summary>
		/// Everything that blocks lighting, but with monsters added.
		/// </summary>
		OpaqueAndNPCs = (Opaque|Contents.Monster),
		
		/// <summary>
		/// Everything that blocks AI line of sight.
		/// </summary>
		BlockLOS = (Contents.Solid|Contents.Moveable|Contents.BlockLOS),
		
		/// <summary>
		/// Everything that blocks AI line of sight in addition to NPCs.
		/// </summary>
		BlockLOSAndNPCs = (BlockLOS|Contents.Monster),
		
		/// <summary>
		/// Everything that blocks players line of sight.
		/// </summary>
		Visible = (Opaque|Contents.IgnoreNodrawOpaque),
		
		/// <summary>
		/// Everything that blocks players line of sight, in addition to monsters.
		/// </summary>
		VisibleAndNPCs = (OpaqueAndNPCs|Contents.IgnoreNodrawOpaque),
		
		/// <summary>
		/// Solid to bullets.
		/// </summary>
		Shot = (Contents.Solid|Contents.Moveable|Contents.Monster|Contents.Window|Contents.Debris|Contents.Hitbox),
		
		/// <summary>
		/// Bullets see these as solid, except monsters (world + brush only).
		/// </summary>
		ShotBrushOnly = (Contents.Solid|Contents.Moveable|Contents.Window|Contents.Debris),
		
		/// <summary>
		/// Non-raycasted weapons see this as solid (includes grates).
		/// </summary>
		ShotHull = (Contents.Solid|Contents.Moveable|Contents.Monster|Contents.Window|Contents.Debris|Contents.Grate),
		
		/// <summary>
		/// Hits solids (not grates) and passes through everything else.
		/// </summary>
		ShotPortal = (Contents.Solid|Contents.Moveable|Contents.Window|Contents.Monster),
		
		/// <summary>
		/// Everything normally solid, except monsters (world + brush only).
		/// </summary>
		SolidBrushOnly = (Contents.Solid|Contents.Moveable|Contents.Window|Contents.Grate),
		
		/// <summary>
		/// Everything normally solid for player movement, except monsters (world + brush only).
		/// </summary>
		PlayerSolidBrushOnly = (Contents.Solid|Contents.Moveable|Contents.Window|Contents.PlayerClip|Contents.Grate),
		
		/// <summary>
		/// Everything normally solid for NPC movement, except monsters (world + brush only).
		/// </summary>
		NPCSolidBrushOnly = (Contents.Solid|Contents.Moveable|Contents.Window|Contents.MonsterClip|Contents.Grate),
		
		/// <summary>
		/// Just the world, used for route rebuilding.
		/// </summary>
		NPCWorldStatic = (Contents.Solid|Contents.Window|Contents.MonsterClip|Contents.Grate),
		
		/// <summary>
		/// Just the world, used for route rebuilding.
		/// </summary>
		NPCWorldStaticFluid = (Contents.Solid|Contents.Window|Contents.MonsterClip),
		
		/// <summary>
		/// Things that can split areaportals.
		/// </summary>
		SplitAreaPortal = (Contents.Water|Contents.Slime),
		
		/// <summary>
		/// UNDONE: Untested, any moving water.
		/// </summary>
		Current = (Contents.Current0|Contents.Current90|Contents.Current180|Contents.Current270|Contents.CurrentUp|Contents.CurrentDown),

		/// <summary>
		/// UNDONE: Not used, everything that blocks corpse movement.
		/// </summary>
		DeadSolid = (Contents.Solid|Contents.PlayerClip|Contents.Window|Contents.Grate),
	}
}