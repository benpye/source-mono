﻿using System;
using System.Reflection;
using Source.Public.Tier0;
using Source.Public.Tier1;
using Source.Shared;

namespace Source.Client
{
	/// <summary>
	/// The script class to be run on the client.
	/// </summary>
    public static class Script
	{
		// On the client we have two different plugin domains

		/// <summary>
		/// UIPlugins are plugins that exist as long as the application does
		/// </summary>
		public static PluginManager UIPlugins;

		/// <summary>
		/// CLPlugins are per game - each time you join a server
		/// </summary>
		public static PluginManager CLPlugins;

		/// <summary>
		/// Init function called by the mono embedding API.
		/// </summary>
		static void Main(string[] args)
		{
			Console.SetOut(new ConsoleWrapper());
			GameEvents.Init();

			UIPlugins = new PluginManager();
			UIPlugins.LoadPlugins(DirectoryHelper.GetMonoDirectory() + "/plugins/ui");
		}

		private static ConCommand printMonoVersion = new ConCommand("cl_mono_version", PrintMonoVersion, "Prints details about the Mono runtime");
		private static ConCommand unloadPluginsMenu = new ConCommand("ui_plugins_unload", PluginsUnloadUI);
		private static ConCommand reloadPluginsMenu = new ConCommand("ui_plugins_reload", PluginsReloadUI);
		private static ConCommand reloadPluginMenu = new ConCommand("ui_plugin_reload", PluginReloadUI);
		private static ConCommand listPluginsMenu = new ConCommand("ui_plugins_list", PluginsListUI);

		private static void PrintMonoVersion(string[] args)
		{
			var type = Type.GetType("Mono.Runtime");
			var dispalayName = type.GetMethod("GetDisplayName", BindingFlags.NonPublic | BindingFlags.Static);
			var monoVersion = (string)dispalayName.Invoke(null, null);
			var netVersion = Environment.Version.ToString();
			var osVersion = Environment.OSVersion.ToString();

			Debug.Msg("Mono: {0}\n", monoVersion);
			Debug.Msg("Runtime: {0}\n", netVersion);
			Debug.Msg("Operating System: {0}\n", osVersion);
		}

		private static void PluginsUnloadUI(string[] args)
		{
			UIPlugins.UnloadPlugins();
		}

		private static void PluginsReloadUI(string[] args)
		{
			UIPlugins.UnloadPlugins();
			UIPlugins.LoadPlugins(DirectoryHelper.GetMonoDirectory() + "/plugins/ui");
		}

		private static void PluginReloadUI(string[] args)
		{
			UIPlugins.ReloadPlugin(args[1]);
		}

		private static void PluginsListUI(string[] args)
		{
			UIPlugins.ListPlugins();
		}
    }
}
