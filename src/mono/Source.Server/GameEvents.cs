﻿using System;
using System.Runtime.InteropServices;
using Source.Public;

namespace Source.Server
{
	/// <summary>
	/// Server implementation of event dispatching.
	/// </summary>
	public class GameEvents
	{
		private delegate void InternalGameEventHandler(IntPtr nativePtr);
		private static InternalGameEventHandler callback;
		[DllImport("__Source")]
		private static extern void set_game_event_callback(InternalGameEventHandler handler);

		/// <summary>
		/// Inits the game event system, adding the callback.
		/// </summary>
		public static void Init()
		{
			callback = GameEventCallback;
			set_game_event_callback(callback);
		}

		/// <summary>
		/// Callback for events, calls the events in the plugins.
		/// </summary>
		/// <param name="nativePtr">Native pointer for the <see cref="EventData"/>.</param>
		private static void GameEventCallback(IntPtr nativePtr)
		{
			var recievedEvent = new EventData(nativePtr);

			if (Script.SVPlugins != null)
				Script.SVPlugins.CallGameEvent(recievedEvent.Name, recievedEvent);
		}
	}
}
