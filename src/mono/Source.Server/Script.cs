﻿using System;
using System.Reflection;
using Source.Public.Tier0;
using Source.Public.Tier1;
using Source.Shared;

namespace Source.Server
{
	/// <summary>
	/// The script class to be run on the server.
	/// </summary>
	public static class Script
	{
		public static PluginManager SVPlugins;

		/// <summary>
		/// Init function called by the mono embedding API.
		/// </summary>
		static void Main(string[] args)
		{
			Console.SetOut(new ConsoleWrapper());
			GameEvents.Init();

			SVPlugins = new PluginManager();
			SVPlugins.LoadPlugins(DirectoryHelper.GetMonoDirectory() + "/plugins/sv");
			SVPlugins.LoadAddons("sv");
		}

		private static ConCommand printMonoVersion = new ConCommand("sv_mono_version", PrintMonoVersion, "Prints details about the Mono runtime");
		private static ConCommand unloadPlugins = new ConCommand("sv_plugins_unload", PluginsUnload);
		private static ConCommand reloadPlugins = new ConCommand("sv_plugins_reload", PluginsReload);
		private static ConCommand reloadPlugin = new ConCommand("sv_plugin_reload", PluginReload);
		private static ConCommand listPlugins = new ConCommand("sv_plugins_list", PluginsList);

		private static void PrintMonoVersion(string[] args)
		{
			var type = Type.GetType("Mono.Runtime");
			var displayName = type.GetMethod("GetDisplayName", BindingFlags.NonPublic | BindingFlags.Static);
			var monoVersion = (string)displayName.Invoke(null, null);
			var netVersion = Environment.Version.ToString();
			var osVersion = Environment.OSVersion.ToString();

			Debug.Msg("Mono: {0}\n", monoVersion);
			Debug.Msg("Runtime: {0}\n", netVersion);
			Debug.Msg("Operating System: {0}\n", osVersion);
		}

		private static void PluginsUnload(string[] args)
		{
			SVPlugins.UnloadPlugins();
		}

		private static void PluginsReload(string[] args)
		{
			SVPlugins.UnloadPlugins();
			SVPlugins.LoadPlugins(DirectoryHelper.GetMonoDirectory() + "/plugins/sv");
			SVPlugins.LoadAddons("sv");
		}

		private static void PluginReload(string[] args)
		{
			SVPlugins.ReloadPlugin(args[1]);
		}

		private static void PluginsList(string[] args)
		{
			SVPlugins.ListPlugins();
		}
	}
}
