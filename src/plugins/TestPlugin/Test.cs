﻿using System;
using System.Reflection;

using Source.Game;
using Source.Interfaces;
using Source.Public.Tier0;
using Source.Public.Tier1;
using Source.Public.Mathlib;
using Source.Public;

namespace TestPlugin
{
	public class Test : IPlugin
	{
		private static ConCommand testEventCmd = new ConCommand("test_event", TestEvent);
		private static ConCommand entTest = new ConCommand("entity_test", EntTestFunc);
		private static ConCommand testCvarGet = new ConCommand("get_cvar_value", TestCvar);
		private static ConCommand traceTest = new ConCommand("trace_test", TraceTester);
		private static ConCommand entSpawnCmd = new ConCommand("ent_spawn_test", EntCreateTest);
		private static ConCommand filesystemAddPathCmd = new ConCommand("fs_add_path", FSAddPath);
 		private static readonly ConVar testConvar = new ConVar("test_convar", "100");

		public void Init()
		{
			Debug.Msg("TestPlugin loaded in domain: {0}\n", AppDomain.CurrentDomain.FriendlyName);
			Events.AddHandler("game_newmap", GameNewMap);
			Events.AddHandler("player_say", PlayerSay);
		}

		~Test()
		{
			Debug.Msg("TestPlugin destroyed in domain: {0}\n", AppDomain.CurrentDomain.FriendlyName);
		}

		public void PlayerSay(EventData ev)
		{
			Debug.Msg("Player: {0} said: {1}\n", ev.GetInt("userid"), ev.GetString("text"));
		}

		public static void TraceTester(string[] args)
		{
			var testTrace = new Trace(new Vector(-7.648900f, 316.987549f, 199.988464f), new Vector(-6.441686f, 38.511143f, 88.231743f));
			Debug.Msg("Trace start: {0}\n", testTrace.StartPosition);
			Debug.Msg("Trace end: {0}\n", testTrace.EndPosition);
			Debug.Msg("Trace hit: {0}\n", testTrace.DidHit);
			Debug.Msg("Trace hit world: {0}\n", testTrace.DidHitWorld);
			Debug.Msg("Trace fraction: {0}\n", testTrace.Fraction);
			Debug.Msg("Trace fraction left solid: {0}\n", testTrace.FractionLeftSolid);
			Debug.Msg("Trace hitnormal: {0}\n", testTrace.HitNormal);
		}

		public static void EntTestFunc(string[] args)
		{
			var entities = Entity.GetAll();
			foreach (var ent in entities)
			{
				Debug.Msg("Entity ClassName {0}\n", ent.ClassName);
				Debug.Msg("Entity Position {0}\n", ent.AbsOrigin);

				if (!ent.IsPlayer) continue;

				Debug.Msg("IS A PLAYER!\n");
				var player =new Player(ent);
				Debug.Msg("Player name {0}\n", player.PlayerName);
				Debug.Msg("Player SteamID 64 {0}\n", player.SteamID64);
				Debug.Msg("Player SteamID {0}\n", player.SteamID);
				player.ClientPrint(HUDPrint.Center, "Center print test!");
				player.ClientPrint(HUDPrint.Console, "Console print test!");
				player.ClientPrint(HUDPrint.Talk, "Talk print test!");
				player.ClientPrint(HUDPrint.Notify, "Notify print test!");
				player.ViewPunch(new Angle(90, 90, 90));
			}
		}

		public static void TestCvar(string[] args)
		{
			Debug.Msg("Cvar as string: {0}\n", testConvar.GetString());
			Debug.Msg("Cvar flags {0}\n", testConvar.Flags);
		}

		public static void TestEvent(string[] args)
		{
			var ev = new EventData("player_changename");
			ev.Set("userid", 1);
			ev.Set("oldname", "dlb1");
			ev.Set("newname", "dlb2");
			ev.FireClientside();
		}

		public static void EntCreateTest(string[] args)
		{
			var createdEnt = Entity.Create("prop_physics");
			createdEnt.Model = "models/weapons/w_smg_mp5.mdl";
			createdEnt.AbsOrigin = new Vector(-7.648900f, 316.987549f, 199.988464f);
			createdEnt.Spawn();
			createdEnt.Activate();
		}

		public void GameNewMap(EventData ev)
		{
			Debug.Msg("New map started:\n");
			Debug.Msg("Map Name: {0}\n", ev.GetString("mapname"));
		}

		public static void FSAddPath(string[] args)
		{
			Filesystem.AddSearchPath(args[1], args[2]);
		}
    }
}
