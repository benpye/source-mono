﻿using System;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;

using Mono.CSharp;
using Source.Game;
using Source.Interfaces;
using Source.Public.Tier0;
using Source.Public.Tier1;
using Source.Public;

namespace CSharpConsole
{
	public class CSharpConsole : IPlugin
    {
		static ConVar csharpasync = new ConVar("csharp_async", "1");
		static string GetMonoLibDirectory()
		{
			var asm = Assembly.GetAssembly(typeof(ConCommand));
			var path = asm.CodeBase;
			var uri = new UriBuilder(path);
			path = Uri.UnescapeDataString(uri.Path);
			path = Path.GetDirectoryName(path);

			return path;
		}

		Evaluator eval;

		public void Init()
		{
			Debug.Msg("C# Console Loaded\n");
			eval = new Evaluator(new CompilerContext(new CompilerSettings(), new ConsoleReportPrinter()));
			var classLibraries = new DirectoryInfo(GetMonoLibDirectory());
			foreach (var file in classLibraries.GetFiles("*.dll"))
			{
				Debug.DevMsg("Referencing {0}\n", file.FullName);
				eval.ReferenceAssembly(Assembly.LoadFrom(file.FullName));
			}
			Events.AddHandler("player_say", CommandHandlerAsync);
		}

		private async void CommandHandlerAsync(EventData ev)
		{
			var textMessage = ev.Get<string>("text");

			if (textMessage == "/csharp-reset")
			{
				eval = new Evaluator(new CompilerContext(new CompilerSettings(), new ConsoleReportPrinter()));
			}
			else if (textMessage.StartsWith("/csharp") && textMessage.Length > 8)
			{
				var csharp = textMessage.Substring(8);

				if(csharpasync.GetBool())
					await Task.Factory.StartNew(async () => RunString(csharp));
				else
					RunString(csharp);
			}
		}

		private void RunString(string csharp)
		{
			Debug.DevMsg("Running {0}\n", csharp);
			try
			{
				eval.Run(csharp);
			}
			catch (Exception e)
			{
				Debug.Warning(String.Format("CSharp Exception: {0}\n", e.Message));
			}
		}
    }
}
