//========= Copyright � 2012 - 2013, Ben Pye, All rights reserved. ==========//
//
// Purpose: Contains the implementation of PNG screenshotting
//
//===========================================================================//

#include "cbase.h"
#include "qlimits.h"
#include "filesystem.h"

/*#ifdef _WIN32
#define ZLIB_WINAPI
#endif

#include "zlib.h"
#include "png.h"*/

#include "miniz.c"

#include "ScreenCapture.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

/*typedef struct
{
   uchar *buffer;
   size_t size;
   size_t size_allocated;
} mem_encode_t;*/

static bool cl_takepng;

static int cl_pngcompression = MZ_BEST_SPEED; //Z_BEST_SPEED;
static ConVar png_compression( "png_compression", "1", 0, "png compression level (higher level smaller file)." );

static int	cl_snapshotnum = 0;
static char cl_snapshotname[MAX_OSPATH];
static char cl_snapshot_subdirname[MAX_OSPATH];

ConVarRef cl_screenshotname( "cl_screenshotname" );

void TakePNG(const char *name, int compression)
{
	cl_takepng = true;
	cl_pngcompression = clamp( compression, 0, 10 ); // max for libpng is 9 not 10

	if ( name != NULL )
	{
		V_strncpy( cl_snapshotname, name, sizeof( cl_snapshotname ) );		
	}
	else
	{
		cl_snapshotname[0] = 0;
		if ( V_strlen( cvar->FindVar("cl_screenshotname")->GetString() ) ) //cl_screenshotname.GetString() returns 0, not a null string
		{
			V_snprintf( cl_snapshotname, sizeof( cl_snapshotname ), "%s", cl_screenshotname.GetString() );		
		}
	}

	cl_snapshot_subdirname[0] = 0;
}

void ReadScreenPixels( int x, int y, int w, int h, void *pBuffer, ImageFormat format )
{
	CMatRenderContextPtr pRenderContext( materials );

	Rect_t rect;
	rect.x = x;
	rect.y = y;
	rect.width = w;
	rect.height = h;

	pRenderContext->ReadPixelsAndStretch( &rect, &rect, (unsigned char*)pBuffer, format, w * ImageLoader::SizeInBytes( format ) );
}

/*static void my_png_write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
	mem_encode_t* p=(mem_encode_t*)png_get_io_ptr(png_ptr);
	size_t nsize = p->size + length;

	//allocate or grow buffer
	if(nsize > p->size_allocated)
	{
		int alloc_size = nsize * 2;
		if(p->buffer)
			p->buffer = (uchar*)realloc(p->buffer, alloc_size);
		else
			p->buffer = (uchar*)malloc(alloc_size);

		if(!p->buffer)
		{
			png_error(png_ptr, "Write Error");
			return;
		}
		p->size_allocated = alloc_size;
	}

	//copy new bytes to end of buffer
	V_memcpy(p->buffer + p->size, data, length);
	p->size += length;
}

static void my_png_flush(png_structp png_ptr){}*/

void TakeScreenshotPNG( const char *pFilename, int compression )
{
	int screenWidth, screenHeight;
	g_pMaterialSystem->GetBackBufferDimensions( screenWidth, screenHeight );

	// bitmap bits
	uint8 *pImage = new uint8[ screenWidth * 3 * screenHeight ];

	// Get Bits from the material system
	ReadScreenPixels( 0, 0, screenWidth, screenHeight, pImage, IMAGE_FORMAT_RGB888 );

	compression = clamp( compression, 0, 9 );
	
	int finalSize = 0;
	//we use miniz, seems libpng is actually slower once miniz has taken one
	size_t out_size;
	void *png = tdefl_write_image_to_png_file_in_memory( pImage, screenWidth, screenHeight, 3, &out_size, cl_pngcompression );

	//libpng version (maybe)
	/*png_structp pp;
	png_infop info;
	png_bytep *row_pointers;
	mem_encode_t state;
	state.buffer = NULL;
	state.size = 0;
	state.size_allocated = 0;

	pp = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	info = png_create_info_struct(pp);

	png_set_write_fn(pp, &state, my_png_write_data, my_png_flush);

	png_set_compression_level(pp, compression);
	png_set_IHDR(pp, info, screenWidth, screenHeight, 8, PNG_COLOR_TYPE_RGB,
		PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	png_set_sRGB(pp, info, PNG_sRGB_INTENT_PERCEPTUAL);
	png_set_sRGB_gAMA_and_cHRM(pp, info, PNG_INFO_sRGB);

	png_write_info(pp, info);

	row_pointers = (png_bytep *)malloc (screenHeight * sizeof (png_byte *));
	for (int y = 0; y < screenHeight; ++y)
	{
		png_bytep row = (png_bytep)malloc (sizeof (uint8) * screenWidth * 3);
		row_pointers[y] = row;
		for (int x = 0; x < screenWidth; ++x)
		{
			*row++ = pImage[(y*screenWidth + x) * 3];
			*row++ = pImage[(y*screenWidth + x) * 3 + 1];
			*row++ = pImage[(y*screenWidth + x) * 3 + 2];
		}
	}

	png_write_image(pp, row_pointers);

	png_write_end(pp, info);
	png_destroy_write_struct(&pp, 0);

	for (int y=0; y<screenHeight; y++)
		free(row_pointers[y]);
	free(row_pointers);

	if(!state.buffer || !state.size)
	{
		Warning("Error writing png file\n");
		if(state.buffer)
			free(state.buffer);
		return;
	}*/

	FileHandle_t fh = filesystem->Open( pFilename, "wb" );
	if ( FILESYSTEM_INVALID_HANDLE != fh )
	{
		//filesystem->Write( state.buffer, state.size, fh );
		filesystem->Write( png, out_size, fh );
		finalSize = filesystem->Tell( fh );
		filesystem->Close( fh );
	}

	// Show info to console.
	char orig[ 64 ];
	char final[ 64 ];
	V_strncpy( orig, V_pretifymem( screenWidth * 3 * screenHeight, 2 ), sizeof( orig ) );
	V_strncpy( final, V_pretifymem( finalSize, 2 ), sizeof( final ) );

	// Let's try and have a similar message to that of the jpeg command, we don't need the quality output
	Msg( "Wrote '%s':  %s (%dx%d) compresssed (compression %i) to %s\n",
		pFilename, orig, screenWidth, screenHeight, compression, final );

	//free(state.buffer);
	mz_free( png );

	delete[] pImage;
}

void TakePNGScreenshot()
{
	if (cl_takepng)
	{
		char base[MAX_OSPATH];
		char filename[MAX_OSPATH];

		filesystem->CreateDirHierarchy( "screenshots", "DEFAULT_WRITE_PATH" );

		//Do we want the map name or just snapshot if we're in a menu
		if ( engine->IsInGame() )
		{
			V_FileBase( engine->GetLevelName(), base, sizeof( base ) );
		}
		else
		{
			V_strncpy( base, "Snapshot", sizeof( base ) );
		}

		char extension[MAX_OSPATH];
		V_snprintf( extension, sizeof( extension ), "%s.%s", GetPlatformExt(), "png" );

		// Using a subdir? If so, create it
		if ( cl_snapshot_subdirname[0] )
		{
			V_snprintf( filename, sizeof( filename ), "screenshots/%s/%s", base, cl_snapshot_subdirname );
			filesystem->CreateDirHierarchy( filename, "DEFAULT_WRITE_PATH" );
		}

		if ( cl_snapshotname[0] )
		{
			V_strncpy( base, cl_snapshotname, sizeof( base ) );
			V_snprintf( filename, sizeof( filename ), "screenshots/%s%s", base, extension );

			int iNumber = 0;
			char renamedfile[MAX_OSPATH];

			while ( 1 )
			{
				V_snprintf( renamedfile, sizeof( renamedfile ), "screenshots/%s_%04d%s", base, iNumber++, extension );	
				if( !filesystem->GetFileTime( renamedfile ) )
					break;
			}

			if (iNumber > 0)
			{
				filesystem->RenameFile(filename, renamedfile);
			}

			cl_screenshotname.SetValue( "" );
		}
		else
		{
			while( 1 )
			{
				if ( cl_snapshot_subdirname[0] )
				{
					V_snprintf( filename, sizeof( filename ), "screenshots/%s/%s/%s%04d%s", base, cl_snapshot_subdirname, base, cl_snapshotnum++, extension  );
				}
				else
				{
					V_snprintf( filename, sizeof( filename ), "screenshots/%s%04d%s", base, cl_snapshotnum++, extension  );
				}

				if( !filesystem->GetFileTime( filename ) )
				{
					// woo hoo!  The file doesn't exist already, so use it.
					break;
				}
			}
		}

		//Take the screenshot
		TakeScreenshotPNG(filename, cl_pngcompression);

		cl_takepng = false;
	}
}

CON_COMMAND_F( png, "Take a png screenshot:  png <filename> <compression 0-9>.", FCVAR_CLIENTCMD_CAN_EXECUTE )
{
	if( args.ArgC() >= 2 )
	{
		if ( args.ArgC() == 3 )
		{
			TakePNG( args[ 1 ], V_atoi( args[2] ) );
		}
		else
		{
			TakePNG( args[ 1 ], png_compression.GetInt() );
		}
	}
	else
	{
		TakePNG( NULL, png_compression.GetInt() );
	}
}