//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Embeds mono and runs the required binaries
//
//===========================================================================//

#include "cbase.h"

#include "monomanager.h"

#include "public/mono_gameevents.h"

#include "mono/jit/jit.h"
#include "mono/metadata/assembly.h"
#include "mono/metadata/mono-debug.h"

#ifdef DEBUG
#include <Windows.h>
#endif

#include "filesystem.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MonoDomain *g_pMonoDomain;

void mono_init()
{
	// Initialize all things that are used in mono
	mono_gameevents_init();
	
#ifdef DEBUG
	mono_debug_init( MONO_DEBUG_FORMAT_MONO );
#ifdef CLIENT_DLL
	if( CommandLine()->CheckParm( "-client-mono-debug" ) )
#else
	if( CommandLine()->CheckParm( "-server-mono-debug" ) )
#endif
	{
		char* options[] = {
			"--soft-breakpoints",
			"--debugger-agent=transport=dt_socket,address=127.0.0.1:10000"
		};

		mono_jit_parse_options( 2, (char **)options );

		DevMsg( "Mono debugging enabled.\n" );
	}
#endif

	char libpath[256];
	filesystem->RelativePathToFullPath( "bin/mono/lib", "MOD", libpath, sizeof(libpath) );
	char etcpath[256];
	filesystem->RelativePathToFullPath( "bin/mono/etc", "MOD", etcpath, sizeof(etcpath) );
	char bindir[256];
#ifdef CLIENT_DLL
	filesystem->RelativePathToFullPath( "mono/lib/client", "MOD", bindir, sizeof(bindir) );
#else
	filesystem->RelativePathToFullPath( "mono/lib/server", "MOD", bindir, sizeof(bindir) );
#endif
	mono_set_dirs( libpath, etcpath );
	mono_set_assemblies_path( bindir );
	DevMsg( "Mono lib directory: %s\n", libpath );
	DevMsg( "Mono etc directory: %s\n", etcpath );
	DevMsg( "Mono assembly search path: %s\n", bindir );

#ifdef CLIENT_DLL
	g_pMonoDomain = mono_jit_init_version( "ClientDomain", "v4.0" );
#elif GAME_DLL
	g_pMonoDomain = mono_jit_init_version( "ServerDomain", "v4.0" );
#endif

	if( !g_pMonoDomain )
	{
		Warning( "Mono initialization failed.\n" );
		return;
	}

#ifdef DEBUG
#ifdef CLIENT_DLL
	if( CommandLine()->CheckParm( "-client-mono-debug" ) )
#else
	if( CommandLine()->CheckParm( "-server-mono-debug" ) )
#endif
	{
		LPTOP_LEVEL_EXCEPTION_FILTER seh_handler = SetUnhandledExceptionFilter(NULL);
		AddVectoredExceptionHandler(1, seh_handler);
		SetUnhandledExceptionFilter(seh_handler);
	}
#endif

	char binpath[256];
	Q_strcpy( binpath, bindir );

#ifdef CLIENT_DLL
	Q_strcat( binpath, "/Source.Client.exe", 255 );
#elif GAME_DLL
	Q_strcat( binpath, "/Source.Server.exe", 255 );
#endif

	MonoAssembly *assembly;

	DevMsg( "Loading mono assembly from %s\n", binpath );
	assembly = mono_domain_assembly_open( g_pMonoDomain, binpath );
	if( !assembly )
	{
		Warning( "Failed to load mono assembly from %s\n", binpath );
		return;
	}

#ifdef GAME_DLL
	char* argvv[1] = { "Source.Server" };
#else
	char* argvv[1] = { "Source.Client" };
#endif

	mono_jit_exec( g_pMonoDomain, assembly, 1, argvv );

	/*MonoImage *image = mono_assembly_get_image( assembly );

	if( !image )
	{
		Warning( "Failed to load image from mono assembly.\n" );
		return;
	}

#ifdef CLIENT_DLL
	MonoClass *klass = mono_class_from_name( image, "Source.Client", "Script" );
#elif GAME_DLL
	MonoClass *klass = mono_class_from_name( image, "Source.Server", "Script" );
#endif

	if( !klass )
	{
		Warning( "Failed to find class in mono image.\n" );
		return;
	}

	MonoMethod *method = mono_class_get_method_from_name( klass, "Init", 0 );

	if( !method )
	{
		Warning( "Failed to find method in mono class.\n" );
		return;
	}

	MonoObject *exception;

	mono_runtime_invoke( method, NULL, NULL, &exception );

	if( exception )
	{
		MonoString *string = mono_object_to_string( exception, NULL );
		Warning( "The managed code thew an exception: %s\n", mono_string_to_utf8( string ) );
	}*/
}

void mono_quit()
{
	mono_jit_cleanup( g_pMonoDomain );
}