//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Util binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"