//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Shared base player function bindings for P/Invoke
// 
//===========================================================================//

#include "cbase.h"

#ifdef CLIENT_DLL
#include "text_message.h"
#include <vgui/ILocalize.h>
#include "vguicenterprint.h"
#include "hud_basechat.h"
#else
#include "basemultiplayerplayer.h"
#endif

#include "steam/steam_api.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// Functions from baseplayer_shared.cpp
MONOAPI Vector *MONOFUNC(baseplayer_get_player_mins)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetPlayerMins() );
}

MONOAPI Vector *MONOFUNC(baseplayer_get_player_maxs)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetPlayerMaxs() );
}

MONOAPI void MONOFUNC(baseplayer_select_item)( CBasePlayer *pObject, char *weaponClass, int subType )
{
	if( pObject == NULL )
		return;

	pObject->SelectItem( weaponClass, subType );
}

MONOAPI void MONOFUNC(baseplayer_view_punch)( CBasePlayer *pObject, QAngle *angleOffset )
{
	if( pObject == NULL || angleOffset == NULL )
		return;

	pObject->ViewPunch( *angleOffset );
}

MONOAPI void MONOFUNC(baseplayer_view_punch_reset)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return;

	pObject->ViewPunchReset();
}

MONOAPI int MONOFUNC(baseplayer_get_default_fov)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetDefaultFOV();
}

MONOAPI int MONOFUNC(baseplayer_get_fov)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetFOV();
}

MONOAPI void MONOFUNC(baseplayer_set_fov)( CBasePlayer *pObject, float fov, int zoomRate )
{
	if( pObject == NULL )
		return;

	pObject->SetFOV( pObject, fov, zoomRate );
}

#ifdef CLIENT_DLL
// Copy paste from sdk_hud_chat so we have the same functionality on the local client
void MsgFunc_TextMsg( char *szString, int msg_dest )
{
	wchar_t szBuf[5][128];
	wchar_t outputBuf[256];

	for ( int i=0; i<5; ++i )
	{
		if ( szString[0] == '^' )
		{
			wchar_t wbuffer[ 64 ];
			g_pVGuiLocalize->ConvertANSIToUnicode( szString, wbuffer, sizeof( wbuffer ) );
			UTIL_ReplaceKeyBindings( wbuffer + 1, sizeof( wbuffer ) - sizeof(*wbuffer), szBuf[i], sizeof( szBuf[i] ) );
		}
		else
		{
			char *tmpStr = hudtextmessage->LookupString( szString, &msg_dest );
			const wchar_t *pBuf = g_pVGuiLocalize->Find( tmpStr );
			if ( pBuf )
			{
				// Copy pBuf into szBuf[i].
				int nMaxChars = sizeof( szBuf[i] ) / sizeof( wchar_t );
				wcsncpy( szBuf[i], pBuf, nMaxChars );
				szBuf[i][nMaxChars-1] = 0;
			}
			else
			{
				if ( i )
				{
					StripEndNewlineFromString( tmpStr );  // these strings are meant for subsitution into the main strings, so cull the automatic end newlines
				}
				g_pVGuiLocalize->ConvertANSIToUnicode( tmpStr, szBuf[i], sizeof(szBuf[i]) );
			}
		}
	}

	if ( !cl_showtextmsg.GetInt() )
		return;

	char outString[512];

	int len;
	switch ( msg_dest )
	{
	case HUD_PRINTCENTER:
		g_pVGuiLocalize->ConstructString( outputBuf, sizeof(outputBuf), szBuf[0], 4, szBuf[1], szBuf[2], szBuf[3], szBuf[4] );
		GetCenterPrint()->Print( ConvertCRtoNL( outputBuf ) );
		break;

	case HUD_PRINTNOTIFY:
		outString[0] = 1;  // mark this message to go into the notify buffer
		g_pVGuiLocalize->ConstructString( outputBuf, sizeof(outputBuf), szBuf[0], 4, szBuf[1], szBuf[2], szBuf[3], szBuf[4] );
		g_pVGuiLocalize->ConvertUnicodeToANSI( outputBuf, outString+1, sizeof(outString)-1 );
		len = strlen( outString );
		if ( len && outString[len-1] != '\n' && outString[len-1] != '\r' )
		{
			Q_strncat( outString, "\n", sizeof(outString), 1 );
		}
		Msg( "%s", ConvertCRtoNL( outString ) );
		break;

	case HUD_PRINTTALK:
		g_pVGuiLocalize->ConstructString( outputBuf, sizeof(outputBuf), szBuf[0], 4, szBuf[1], szBuf[2], szBuf[3], szBuf[4] );
		g_pVGuiLocalize->ConvertUnicodeToANSI( outputBuf, outString, sizeof(outString) );
		len = strlen( outString );
		if ( len && outString[len-1] != '\n' && outString[len-1] != '\r' )
		{
			Q_strncat( outString, "\n", sizeof(outString), 1 );
		}
		if( CBaseHudChat::GetHudChat() )
			CBaseHudChat::GetHudChat()->ChatPrintf( 0, CHAT_FILTER_SERVERMSG, "%s", ConvertCRtoNL( outString ) );
		break;

	case HUD_PRINTCONSOLE:
		g_pVGuiLocalize->ConstructString( outputBuf, sizeof(outputBuf), szBuf[0], 4, szBuf[1], szBuf[2], szBuf[3], szBuf[4] );
		g_pVGuiLocalize->ConvertUnicodeToANSI( outputBuf, outString, sizeof(outString) );
		len = strlen( outString );
		if ( len && outString[len-1] != '\n' && outString[len-1] != '\r' )
		{
			Q_strncat( outString, "\n", sizeof(outString), 1 );
		}
		Msg( "%s", ConvertCRtoNL( outString ) );
		break;
	}
}
#endif

MONOAPI void MONOFUNC(baseplayer_client_print)( CBasePlayer *pObject, int msgDest, char *msg )
{
	if( pObject == NULL )
		return;

#ifdef GAME_DLL
	ClientPrint( pObject, msgDest, msg );
#else
	if( pObject == CBasePlayer::GetLocalPlayer() )
		MsgFunc_TextMsg( msg, msgDest );
#endif
}

MONOAPI int MONOFUNC(baseplayer_get_user_id)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return 0;
	
	return pObject->GetUserID();
}

MONOAPI uint64 MONOFUNC(baseplayer_get_steam_id)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return 0;

#ifdef CLIENT_DLL
	player_info_t pi;
	engine->GetPlayerInfo( pObject->entindex(), &pi );
	CSteamID steamIDForPlayer( pi.friendsID, 1, steamapicontext->SteamUtils()->GetConnectedUniverse(), k_EAccountTypeIndividual );
	return steamIDForPlayer.ConvertToUint64();
#else
	const CSteamID *steamIDForPlayer = engine->GetClientSteamID( pObject->edict() );
	return steamIDForPlayer->ConvertToUint64();
#endif
}

MONOAPI char *MONOFUNC(baseplayer_get_player_name)( CBasePlayer *pObject )
{
	if( pObject == NULL )
		return "";
	
	return (char *)pObject->GetPlayerName();
}