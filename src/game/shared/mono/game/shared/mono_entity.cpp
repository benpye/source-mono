//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Shared entitylist function bindings for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "entitylist_base.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

typedef struct
{
	CBaseEntity **ents;
	int count;
} EntStruct_t;

MONOAPI EntStruct_t MONOFUNC(entity_get_all)()
{
	CUtlVector<CBaseEntity *> ents;
	ents.RemoveAll();
	CBaseHandle hCur = g_pEntityList->FirstHandle();
	while ( hCur != g_pEntityList->InvalidHandle() )
	{
		CBaseEntity *ent = EntityFromEntityHandle( hCur.Get() );

		if(ent != NULL)
			ents.AddToHead( ent );

		hCur = g_pEntityList->NextHandle( hCur );
	}

	EntStruct_t entsToPass;
	entsToPass.count = ents.Count();
	entsToPass.ents = new CBaseEntity *[entsToPass.count];
	FOR_EACH_VEC( ents, i )
	{
		entsToPass.ents[i] = ents[i];
	}

	return entsToPass;
}

MONOAPI EntStruct_t MONOFUNC(entity_get_in_sphere)( int maxCount, Vector* center, float radius, int flagMask )
{
	CBaseEntity **ents = new CBaseEntity*[maxCount];
	int count = UTIL_EntitiesInSphere( ents, maxCount, *center, radius, flagMask );

	EntStruct_t entsToPass;
	entsToPass.count = count;
	entsToPass.ents = new CBaseEntity *[entsToPass.count];

	for(int i = 0; i < count; i++)
	{
		entsToPass.ents[i] = ents[i];
	}

	return entsToPass;
}

MONOAPI EntStruct_t MONOFUNC(entity_get_in_box)( int maxCount, Vector* mins, Vector* maxs, int flagMask )
{
	CBaseEntity **ents = new CBaseEntity*[maxCount];
	int count = UTIL_EntitiesInBox( ents, maxCount, *mins, *maxs, flagMask );

	EntStruct_t entsToPass;
	entsToPass.count = count;
	entsToPass.ents = new CBaseEntity *[entsToPass.count];

	for(int i = 0; i < count; i++)
	{
		entsToPass.ents[i] = ents[i];
	}

	return entsToPass;
}

MONOAPI CBaseEntity *MONOFUNC(entity_create)( char *classname )
{
	return CreateEntityByName( classname );
}

MONOAPI void MONOFUNC(entity_delete_struct)( EntStruct_t entStruct )
{
	delete [] entStruct.ents;
}

MONOAPI CBaseEntity *MONOFUNC(entity_get_by_index)( int id )
{
#ifdef GAME_DLL
	return UTIL_EntityByIndex( id );
#else
	return ClientEntityList().GetBaseEntity( id );
#endif
}