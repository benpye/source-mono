//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Shared entity function bindings for P/Invoke
// 
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MONOAPI bool MONOFUNC(engine_is_client)()
{
	return gpGlobals->IsClient();
}

MONOAPI float MONOFUNC(engine_real_time)()
{
	return gpGlobals->realtime;
}

MONOAPI int MONOFUNC(engine_frame_count)()
{
	return gpGlobals->framecount;
}

MONOAPI float MONOFUNC(engine_absolute_frame_time)()
{
	return gpGlobals->absoluteframetime;
}

MONOAPI float MONOFUNC(engine_cur_time)()
{
	return gpGlobals->curtime;
}

MONOAPI float MONOFUNC(engine_frame_time)()
{
	return gpGlobals->frametime;
}

MONOAPI int MONOFUNC(engine_max_clients)()
{
	return gpGlobals->maxClients;
}

MONOAPI int MONOFUNC(engine_tick_count)()
{
	return gpGlobals->tickcount;
}

MONOAPI float MONOFUNC(engine_interval_per_tick)()
{
	return gpGlobals->interval_per_tick;
}

MONOAPI float MONOFUNC(engine_interpolation_amount)()
{
	return gpGlobals->interpolation_amount;
}

MONOAPI int MONOFUNC(engine_sim_ticks_this_frame)()
{
	return gpGlobals->simTicksThisFrame;
}

MONOAPI int MONOFUNC(engine_network_protocol)()
{
	return gpGlobals->network_protocol;
}