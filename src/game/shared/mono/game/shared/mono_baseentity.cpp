//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Shared base entity function bindings for P/Invoke
// 
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// Functions from baseentity_shared.h
MONOAPI void MONOFUNC(baseentity_clear_effects)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->ClearEffects();
}

MONOAPI float MONOFUNC(baseentity_get_anim_time)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetAnimTime();
}

MONOAPI int MONOFUNC(baseentity_get_collision_group)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetCollisionGroup();
}

MONOAPI CBaseEntity *MONOFUNC(baseentity_get_effect_entity)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->GetEffectEntity();
}

MONOAPI int MONOFUNC(baseentity_get_effects)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetEffects();
}

MONOAPI int MONOFUNC(baseentity_get_flags)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetFlags();
}

MONOAPI int MONOFUNC(baseentity_get_move_collide)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetMoveCollide();
}

MONOAPI int MONOFUNC(baseentity_get_move_type)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetMoveType();
}

MONOAPI CBaseEntity *MONOFUNC(baseentity_get_owner_entity)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->GetOwnerEntity();
}

MONOAPI CBasePlayer *MONOFUNC(baseentity_get_prediction_player)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->GetPredictionPlayer();
}

MONOAPI int MONOFUNC(baseentity_get_prediction_random_seed)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetPredictionRandomSeed();
}

MONOAPI CBasePlayer *MONOFUNC(baseentity_get_simulating_player)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->GetSimulatingPlayer();
}

MONOAPI float MONOFUNC(baseentity_get_simulation_time)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetSimulationTime();
}

MONOAPI bool MONOFUNC(baseentity_is_alive)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsAlive();
}

MONOAPI bool MONOFUNC(baseentity_is_animated_every_tick)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsAnimatedEveryTick();
}

MONOAPI bool MONOFUNC(baseentity_is_effect_active)( CBaseEntity *pObject, int effect )
{
	if( pObject == NULL )
		return false;

	return pObject->IsEffectActive( effect );
}

MONOAPI bool MONOFUNC(baseentity_is_player_simulated)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsPlayerSimulated();
}

MONOAPI bool MONOFUNC(baseentity_is_simulated_every_tick)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsSimulatedEveryTick();
}

MONOAPI void MONOFUNC(baseentity_remove_effects)( CBaseEntity *pObject, int effects )
{
	if( pObject == NULL )
		return;

	pObject->RemoveEffects( effects );
}

MONOAPI void MONOFUNC(baseentity_set_anim_time)( CBaseEntity *pObject, float time )
{
	if( pObject == NULL )
		return;

	pObject->SetAnimTime( time );
}

MONOAPI void MONOFUNC(baseentity_set_animated_every_tick)( CBaseEntity *pObject, bool anim )
{
	if( pObject == NULL )
		return;

	pObject->SetAnimatedEveryTick( anim );
}

MONOAPI void MONOFUNC(baseentity_set_prediction_player)( CBaseEntity *pObject, CBasePlayer *player )
{
	if( pObject == NULL || player == NULL )
		return;

	pObject->SetPredictionPlayer( player );
}

MONOAPI void MONOFUNC(baseentity_set_simulated_every_tick)( CBaseEntity *pObject, bool sim )
{
	if( pObject == NULL )
		return;

	pObject->SetSimulatedEveryTick( sim );
}

MONOAPI void MONOFUNC(baseentity_set_simulation_time)( CBaseEntity *pObject, float st )
{
	if( pObject == NULL )
		return;

	pObject->SetSimulationTime( st );
}

// Functions from baseentity_shared.cpp

MONOAPI void MONOFUNC(baseentity_add_effects)( CBaseEntity *pObject, int effects )
{
	if( pObject == NULL )
		return;

	pObject->AddEffects( effects );
}

MONOAPI void MONOFUNC(baseentity_add_flag)( CBaseEntity *pObject, int flag )
{
	if( pObject == NULL )
		return;

	pObject->AddFlag( flag );
}

MONOAPI void MONOFUNC(baseentity_apply_abs_velocity_impulse)( CBaseEntity *pObject, Vector *impulse )
{
	if( pObject == NULL || impulse == NULL )
		return;

	pObject->ApplyAbsVelocityImpulse( *impulse );
}

MONOAPI void MONOFUNC(baseentity_apply_local_angular_velocity_impulse)( CBaseEntity *pObject, Vector *impulse )
{
	if( pObject == NULL || impulse == NULL )
		return;

	pObject->ApplyLocalAngularVelocityImpulse( *impulse );
}

MONOAPI void MONOFUNC(baseentity_apply_local_velocity_impulse)( CBaseEntity *pObject, Vector *impulse )
{
	if( pObject == NULL || impulse == NULL )
		return;

	pObject->ApplyLocalVelocityImpulse( *impulse );
}

MONOAPI bool MONOFUNC(baseentity_blocks_los)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->BlocksLOS();
}

MONOAPI int MONOFUNC(baseentity_blood_color)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->BloodColor();
}

//MOVE TO CLIENT
/*MONOAPI void MONOFUNC(baseentity_check_has_game_physics_simulation)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->CheckHasGamePhysicsSimulation();
}*/

//MOVE TO CLIENT
/*MONOAPI void MONOFUNC(baseentity_check_has_thick_function)( CBaseEntity *pObject, bool isThinking )
{
	if( pObject == NULL )
		return;

	pObject->CheckHasThinkFunction( isThinking );
}*/

MONOAPI void MONOFUNC(baseentity_clear_flags)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->ClearFlags();
}

MONOAPI void MONOFUNC(baseentity_collision_rules_changed)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->CollisionRulesChanged();
}

//MOVE TO CLIENT
/*MONOAPI Vector *MONOFUNC(baseentity_compute_tracer_start_position)( CBaseEntity *pObject, Vector *src )
{
	if( pObject == NULL || src == NULL )
		return NULL;

	Vector *vecTracerSrc = new Vector();

	pObject->ComputeTracerStartPosition( *src, vecTracerSrc );

	return vecTracerSrc;
}*/

MONOAPI bool MONOFUNC(baseentity_create_vphysics)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->CreateVPhysics();
}

MONOAPI char *MONOFUNC(baseentity_damage_decal)( CBaseEntity *pObject, int damageType, int material )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->DamageDecal( damageType, material );
}

MONOAPI void MONOFUNC(baseentity_decal_trace)( CBaseEntity *pObject, trace_t *trace, char *name )
{
	if( pObject == NULL || trace == NULL )
		return;

	pObject->DecalTrace( trace, name );
}

MONOAPI void MONOFUNC(baseentity_dispatch_trace_attack)( CBaseEntity *pObject, CTakeDamageInfo *dInfo,
														Vector *dir, trace_t *trace )
{
	if( pObject == NULL || dInfo == NULL || dir == NULL || trace == NULL )
		return;

	pObject->DispatchTraceAttack( *dInfo, *dir, trace );
}

MONOAPI void MONOFUNC(baseentity_do_impact_effect)( CBaseEntity *pObject, trace_t *trace, int damageType )
{
	if( pObject == NULL || trace == NULL )
		return;

	pObject->DoImpactEffect( *trace, damageType );
}

MONOAPI Vector *MONOFUNC(baseentity_ear_position)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->EarPosition() );
}

MONOAPI QAngle *MONOFUNC(baseentity_eye_angles)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new QAngle( pObject->EyeAngles() );
}

MONOAPI Vector *MONOFUNC(baseentity_eye_position)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->EyePosition() );
}

MONOAPI void MONOFUNC(baseentity_fire_bullets)( CBaseEntity *pObject, int nShots, Vector *vecSrc, 
					Vector *vecDir, Vector *vecSpread, float flDistance, int nAmmoType, bool bPrimaryAttack )
{
	if( pObject == NULL || vecSrc == NULL || vecDir == NULL || vecSpread == NULL )
		return;

	FireBulletsInfo_t fireBullets( nShots, *vecSrc, *vecDir, *vecSpread, flDistance, nAmmoType, bPrimaryAttack );

	pObject->FireBullets( fireBullets );
}

MONOAPI void MONOFUNC(baseentity_follow_entity)( CBaseEntity *pObject, CBaseEntity *other, bool boneMerge )
{
	if( pObject == NULL || other == NULL )
		return;

	pObject->FollowEntity( other, boneMerge );
}

MONOAPI int MONOFUNC(baseentity_get_first_think_tick)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetFirstThinkTick();
}

MONOAPI float MONOFUNC(baseentity_get_last_think)( CBaseEntity *pObject, char *context )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetLastThink( context );
}

MONOAPI int MONOFUNC(baseentity_get_last_think_tick)( CBaseEntity *pObject, char *context )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetLastThinkTick( context );
}

MONOAPI float MONOFUNC(baseentity_get_next_think)( CBaseEntity *pObject, char *context )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetNextThink( context );
}

MONOAPI int MONOFUNC(baseentity_get_next_think_tick)( CBaseEntity *pObject, char *context )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetNextThinkTick( context );
}

MONOAPI CBaseEntity *MONOFUNC(baseentity_get_root_move_parent)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->GetRootMoveParent();
}

MONOAPI int MONOFUNC(baseentity_get_tracer_attachment)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetTracerAttachment();
}

MONOAPI char *MONOFUNC(baseentity_get_tracer_type)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetTracerType();
}

MONOAPI Vector *MONOFUNC(baseentity_get_view_offset)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetViewOffset() );
}

MONOAPI int MONOFUNC(baseentity_get_water_type)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetWaterType();
}

//MOVE TO CLIENT
/*MONOAPI bool MONOFUNC(baseentity_handle_shot_impacting_water)( CBaseEntity *pObject, int nShots, Vector *vecSrc,
						Vector *vecDir, Vector *vecSpread, float flDistance, int nAmmoType, bool bPrimaryAttack,
						Vector *vecEnd, ITraceFilter *traceFilter, Vector *vecTracerDest )
{
	if( pObject == NULL || vecSrc == NULL || vecDir == NULL || vecSpread == NULL || vecEnd == NULL
		|| traceFilter == NULL || vecTracerDest == NULL )
		return false;

	FireBulletsInfo_t fireBullets( nShots, *vecSrc, *vecDir, *vecSpread, flDistance, nAmmoType, bPrimaryAttack );

	return pObject->HandleShotImpactingWater( fireBullets, *vecEnd, traceFilter, vecTracerDest );
}*/

MONOAPI void MONOFUNC(baseentity_impact_trace)( CBaseEntity *pObject, trace_t *trace, int damageType, char *impactName )
{
	if( pObject == NULL || trace == NULL )
		return;

	pObject->ImpactTrace( trace, damageType, impactName );
}

//MOVE TO CLIENT
/*MONOAPI void MONOFUNC(baseentity_invalidate_physics_recursive)( CBaseEntity *pObject, int changeFlags )
{
	if( pObject == NULL )
		return;

	return pObject->InvalidatePhysicsRecursive( changeFlags );
}*/

MONOAPI bool MONOFUNC(baseentity_is_ai_walkable)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsAIWalkable();
}

MONOAPI bool MONOFUNC(baseentity_is_bsp_model)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsBSPModel();
}

MONOAPI bool MONOFUNC(baseentity_is_precache_allowed)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsPrecacheAllowed();
}

MONOAPI bool MONOFUNC(baseentity_is_simulating_on_alternate_ticks)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsSimulatingOnAlternateTicks();
}

MONOAPI bool MONOFUNC(baseentity_is_standable)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsStandable();
}

//MOVE TO CLIENT
/*MONOAPI bool MONOFUNC(baseentity_is_tool_recording)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsToolRecording();
}*/

MONOAPI QAngle *MONOFUNC(baseentity_local_eye_angles)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new QAngle( pObject->LocalEyeAngles() );
}

MONOAPI void MONOFUNC(baseentity_make_trace)( CBaseEntity *pObject, Vector *vecTracerSrc, trace_t *trace, int tracerType )
{
	if( pObject == NULL || vecTracerSrc == NULL || trace == NULL )
		return;

	pObject->MakeTracer( *vecTracerSrc, *trace, tracerType );
}

MONOAPI void MONOFUNC(baseentity_physics_touch_triggers)( CBaseEntity *pObject, Vector *prevAbsOrigin )
{
	if( pObject == NULL || prevAbsOrigin == NULL )
		return;

	pObject->PhysicsTouchTriggers( prevAbsOrigin );
}

MONOAPI int MONOFUNC(baseentity_register_think_context)( CBaseEntity *pObject, char *context )
{
	if( pObject == NULL )
		return 0;

	return pObject->RegisterThinkContext( context );
}

MONOAPI void MONOFUNC(baseentity_remove_flag)( CBaseEntity *pObject, int flagsToRemove )
{
	if( pObject == NULL )
		return;

	pObject->RemoveFlag( flagsToRemove );
}

MONOAPI void MONOFUNC(baseentity_set_ai_walkable)( CBaseEntity *pObject, bool blocksLOS )
{
	if( pObject == NULL )
		return;

	pObject->SetAIWalkable( blocksLOS );
}

MONOAPI void MONOFUNC(baseentity_set_allow_precache)( CBaseEntity *pObject, bool allow )
{
	if( pObject == NULL )
		return;

	pObject->SetAllowPrecache( allow );
}

MONOAPI void MONOFUNC(baseentity_set_blocks_los)( CBaseEntity *pObject, bool blocksLOS )
{
	if( pObject == NULL )
		return;

	pObject->SetBlocksLOS( blocksLOS );
}

MONOAPI void MONOFUNC(baseentity_set_collision_group)( CBaseEntity *pObject, int group )
{
	if( pObject == NULL )
		return;

	pObject->SetCollisionGroup( group );
}

MONOAPI void MONOFUNC(baseentity_set_effect_entity)( CBaseEntity *pObject, CBaseEntity *effectEntity )
{
	if( pObject == NULL || effectEntity == NULL )
		return;

	pObject->SetEffectEntity( effectEntity );
}

MONOAPI void MONOFUNC(baseentity_set_effects)( CBaseEntity *pObject, int effects )
{
	if( pObject == NULL )
		return;

	pObject->SetEffects( effects );
}

MONOAPI void MONOFUNC(baseentity_set_next_think)( CBaseEntity *pObject, float thinkTime, char *context )
{
	if( pObject == NULL )
		return;

	pObject->SetNextThink( thinkTime, context );
}

MONOAPI void MONOFUNC(baseentity_set_player_simulated)( CBaseEntity *pObject, CBasePlayer *player )
{
	if( pObject == NULL || player == NULL )
		return;

	pObject->SetPlayerSimulated( player );
}

MONOAPI void MONOFUNC(baseentity_set_prediction_random_seed)( CBaseEntity *pObject, int seed )
{
	if( pObject == NULL )
		return;

	CUserCmd cmd;
	cmd.random_seed = seed;
	pObject->SetPredictionRandomSeed( &cmd );
}

MONOAPI void MONOFUNC(baseentity_set_view_offset)( CBaseEntity *pObject, Vector *offset )
{
	if( pObject == NULL || offset == NULL )
		return;

	pObject->SetViewOffset( *offset );
}

MONOAPI void MONOFUNC(baseentity_set_water_type)( CBaseEntity *pObject, int type )
{
	if( pObject == NULL )
		return;

	pObject->SetWaterType( type );
}

MONOAPI bool MONOFUNC(baseentity_should_collide)( CBaseEntity *pObject, int collisionGroup, int contentsMask )
{
	if( pObject == NULL )
		return false;

	return pObject->ShouldCollide( collisionGroup, contentsMask );
}

//MOVE TO CLIENT
/*MONOAPI bool MONOFUNC(baseentity_should_draw_underwater_bullet_bubbles)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->ShouldDrawUnderwaterBulletBubbles();
}*/

MONOAPI BASEPTR MONOFUNC(baseentity_think_set)( CBaseEntity *pObject, BASEPTR func, float nextThinkTime, char *context )
{
	if( pObject == NULL )
		return NULL;

	return pObject->ThinkSet( func, nextThinkTime, context );
}

MONOAPI void MONOFUNC(baseentity_toggle_flag)( CBaseEntity *pObject, int flag )
{
	if( pObject == NULL )
		return;

	pObject->ToggleFlag( flag );
}

//MOVE TO CLIENT
/*MONOAPI void MONOFUNC(baseentity_trace_attack)( CBaseEntity *pObject, CTakeDamageInfo *dInfo,
														Vector *dir, trace_t *trace )
{
	if( pObject == NULL || dInfo == NULL || dir == NULL || trace == NULL )
		return;

	pObject->TraceAttack( *dInfo, *dir, trace );
}*/

MONOAPI void MONOFUNC(baseentity_trace_bleed)( CBaseEntity *pObject, float damage,
														Vector *dir, trace_t *trace, int damageType )
{
	if( pObject == NULL || dir == NULL || trace == NULL )
		return;

	pObject->TraceBleed( damage, *dir, trace, damageType );
}

MONOAPI void MONOFUNC(baseentity_unset_player_simulated)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->UnsetPlayerSimulated();
}

MONOAPI void MONOFUNC(baseentity_vphysics_destroy_object)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->VPhysicsDestroyObject();
}

MONOAPI IPhysicsObject *MONOFUNC(baseentity_vphysics_init_normal)( CBaseEntity *pObject, int solidType, int solidFlags,
																  bool createAsleep, solid_t *solid )
{
	if( pObject == NULL || solid == NULL )
		return NULL;

	return pObject->VPhysicsInitNormal( (SolidType_t)solidType, solidFlags, createAsleep, solid );
}

MONOAPI IPhysicsObject *MONOFUNC(baseentity_vphysics_init_shadow)( CBaseEntity *pObject, bool allowPhysicsMove,
													   bool allowPhysicsRot, solid_t *solid )
{
	if( pObject == NULL || solid == NULL )
		return NULL;

	return pObject->VPhysicsInitShadow( allowPhysicsMove, allowPhysicsRot, solid );
}

MONOAPI IPhysicsObject *MONOFUNC(baseentity_vphysics_init_static)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->VPhysicsInitStatic();
}

MONOAPI void MONOFUNC(baseentity_vphysics_set_object)( CBaseEntity *pObject, IPhysicsObject *physObj )
{
	if( pObject == NULL || physObj == NULL )
		return;

	pObject->VPhysicsSetObject( physObj );
}

MONOAPI void MONOFUNC(baseentity_vphysics_set_update)( CBaseEntity *pObject, IPhysicsObject *physObj )
{
	if( pObject == NULL || physObj == NULL )
		return;

	pObject->VPhysicsUpdate( physObj );
}

//MOVE TO CLIENT
/*MONOAPI bool MONOFUNC(baseentity_vphysics_will_simulate_game_physics)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->WillSimulateGamePhysics();
}*/

//MOVE TO CLIENT
/*MONOAPI bool MONOFUNC(baseentity_will_think)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->WillThink();
}*/

MONOAPI Vector *MONOFUNC(baseentity_world_space_center)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->WorldSpaceCenter() );
}

// Useful shared functions

MONOAPI char *MONOFUNC(baseentity_get_classname)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetClassname();
}

MONOAPI char *MONOFUNC(baseentity_get_debug_name)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetDebugName();
}

MONOAPI char *MONOFUNC(baseentity_get_model)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return "";

#ifndef NO_STRING_T
	return (char *)pObject->GetModelName().ToCStr();
#else
	return (char *)pObject->GetModelName();
#endif
}

MONOAPI void MONOFUNC(baseentity_set_model)( CBaseEntity *pObject, char *model )
{
	if( pObject == NULL )
		return;

	pObject->SetModel( model );
}

MONOAPI Vector *MONOFUNC(baseentity_get_abs_origin)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetAbsOrigin() );
}

MONOAPI QAngle *MONOFUNC(baseentity_get_abs_angles)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new QAngle( pObject->GetAbsAngles() );
}

MONOAPI void MONOFUNC(baseentity_set_abs_origin)( CBaseEntity *pObject, Vector *origin )
{
	if( pObject == NULL )
		return;

	pObject->SetAbsOrigin( *origin );
}

MONOAPI void MONOFUNC(baseentity_set_abs_angles)( CBaseEntity *pObject, QAngle *angles )
{
	if( pObject == NULL )
		return;

	pObject->SetAbsAngles( *angles );
}

MONOAPI Vector *MONOFUNC(baseentity_get_local_origin)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetLocalOrigin() );
}

MONOAPI QAngle *MONOFUNC(baseentity_get_local_angles)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new QAngle( pObject->GetLocalAngles() );
}

MONOAPI void MONOFUNC(baseentity_set_local_origin)( CBaseEntity *pObject, Vector *origin )
{
	if( pObject == NULL )
		return;

	pObject->SetLocalOrigin( *origin );
}

MONOAPI void MONOFUNC(baseentity_set_local_angles)( CBaseEntity *pObject, QAngle *angles )
{
	if( pObject == NULL )
		return;

	pObject->SetLocalAngles( *angles );
}

MONOAPI Vector *MONOFUNC(baseentity_get_abs_velocity)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetAbsVelocity() );
}

MONOAPI void MONOFUNC(baseentity_set_abs_velocity)( CBaseEntity *pObject, Vector *origin )
{
	if( pObject == NULL )
		return;

	pObject->SetAbsVelocity( *origin );
}

MONOAPI Vector *MONOFUNC(baseentity_get_local_velocity)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->GetLocalVelocity() );
}

MONOAPI void MONOFUNC(baseentity_set_local_velocity)( CBaseEntity *pObject, Vector *origin )
{
	if( pObject == NULL )
		return;

	pObject->SetLocalVelocity( *origin );
}

MONOAPI Vector *MONOFUNC(baseentity_up)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->Up() );
}

MONOAPI Vector *MONOFUNC(baseentity_forward)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->Forward() );
}

MONOAPI Vector *MONOFUNC(baseentity_left)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->Left() );
}

MONOAPI Vector *MONOFUNC(baseentity_world_align_mins)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->WorldAlignMins() );
}

MONOAPI Vector *MONOFUNC(baseentity_world_align_maxs)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->WorldAlignMaxs() );
}

MONOAPI float MONOFUNC(baseentity_emit_sound)( CBaseEntity *pObject, char *soundname, float time )
{
	if( pObject == NULL )
		return 0.0f;

	float duration;
	pObject->EmitSound( (const char *)soundname, time, &duration );
	return duration;
}

//SHOULD BE SERVERSIDE
/*MONOAPI float MONOFUNC(baseentity_get_friction)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetFriction();
}*/

MONOAPI void MONOFUNC(baseentity_set_friction)( CBaseEntity *pObject, float friction )
{
	if( pObject == NULL )
		return;

	pObject->SetFriction( friction );
}

MONOAPI float MONOFUNC(baseentity_get_gravity)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetGravity();
}

MONOAPI void MONOFUNC(baseentity_set_gravity)( CBaseEntity *pObject, float gravity )
{
	if( pObject == NULL )
		return;

	pObject->SetGravity( gravity );
}

MONOAPI bool MONOFUNC(baseentity_is_player)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsPlayer();
}

MONOAPI bool MONOFUNC(baseentity_is_world)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsWorld();
}

MONOAPI bool MONOFUNC(baseentity_is_in_world)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsInWorld();
}

MONOAPI bool MONOFUNC(baseentity_is_weapon)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsBaseCombatWeapon();
}

MONOAPI bool MONOFUNC(baseentity_is_npc)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsNPC();
}

MONOAPI int MONOFUNC(baseentity_get_health)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetHealth();
}

MONOAPI int MONOFUNC(baseentity_get_max_health)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetMaxHealth();
}

MONOAPI void MONOFUNC(baseentity_set_health)( CBaseEntity *pObject, int health )
{
	if( pObject == NULL )
		return;

	pObject->SetHealth( health );
}

MONOAPI int MONOFUNC(baseentity_get_eflags)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetEFlags();
}

MONOAPI void MONOFUNC(baseentity_set_eflags)( CBaseEntity *pObject, int flags )
{
	if( pObject == NULL )
		return;

	pObject->SetEFlags( flags );
}

MONOAPI void MONOFUNC(baseentity_add_eflags)( CBaseEntity *pObject, int flags )
{
	if( pObject == NULL )
		return;

	pObject->AddEFlags( flags );
}

MONOAPI void MONOFUNC(baseentity_remove_eflags)( CBaseEntity *pObject, int flags )
{
	if( pObject == NULL )
		return;

	pObject->RemoveEFlags( flags );
}

MONOAPI bool MONOFUNC(baseentity_is_eflag_set)( CBaseEntity *pObject, int flags )
{
	if( pObject == NULL )
		return false;

	return pObject->IsEFlagSet( flags );
}

MONOAPI int MONOFUNC(baseentity_get_ent_index)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->entindex();
}

// Functions for spawning ents
MONOAPI void MONOFUNC(baseentity_activate)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

	pObject->Activate();
}

MONOAPI void MONOFUNC(baseentity_spawn)( CBaseEntity *pObject )
{
	if( pObject == NULL )
		return;

#ifdef GAME_DLL
	DispatchSpawn( pObject );
#else
	pObject->Spawn();
#endif
}