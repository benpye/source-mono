//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Util binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#ifndef CLIENT_DLL 
#include "player.h"
#endif

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MONOAPI CBasePlayer *MONOFUNC(player_get_local)()
{
#ifdef CLIENT_DLL
	return CBasePlayer::GetLocalPlayer();
#else
	return UTIL_GetLocalPlayerOrListenServerHost();
#endif
}

MONOAPI CBasePlayer *MONOFUNC(player_get_by_index)( int index )
{
	return UTIL_PlayerByIndex( index );
}

MONOAPI CBasePlayer *MONOFUNC(player_get_by_userid)( int userID )
{
	return UTIL_PlayerByUserId( userID );
}