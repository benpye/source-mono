//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Trace binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

/*MONOAPI trace_t *MONOFUNC(trace_create_class)()
{
	return new trace_t();
}*/

MONOAPI trace_t *MONOFUNC(trace_trace_line)( Vector *vecAbsStart, Vector *vecAbsEnd, unsigned int mask, 
					 CBaseEntity **filterEnts, int entCount )
{
	if( vecAbsStart == NULL || vecAbsEnd == NULL )
		return NULL;

	trace_t *retTrace = new trace_t();

	CTraceFilterSimpleList filter( COLLISION_GROUP_NONE );

	if( entCount > 0 )
		filter.AddEntitiesToIgnore( entCount, (IHandleEntity **)filterEnts );

	UTIL_TraceLine( *vecAbsStart, *vecAbsEnd, mask, &filter, retTrace );

	return retTrace;
}

MONOAPI trace_t *MONOFUNC(trace_trace_hull)( Vector *vecAbsStart, Vector *vecAbsEnd, Vector *hullMin, Vector *hullMax,
											unsigned int mask, CBaseEntity **filterEnts, int entCount )
{
	if( vecAbsStart == NULL || vecAbsEnd == NULL )
		return NULL;

	trace_t *retTrace = new trace_t();

	CTraceFilterSimpleList filter( COLLISION_GROUP_NONE );
	
	if( entCount > 0 )
		filter.AddEntitiesToIgnore( entCount, (IHandleEntity **)filterEnts );

	UTIL_TraceHull( *vecAbsStart, *vecAbsEnd, *hullMin, *hullMax, mask, &filter, retTrace );

	return retTrace;
}

MONOAPI void MONOFUNC(trace_destroy_class)( trace_t *pObject )
{
	if( pObject == NULL )
		return;

	delete pObject;
}

MONOAPI bool MONOFUNC(trace_did_hit)( trace_t *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->DidHit();
}

MONOAPI bool MONOFUNC(trace_did_hit_world)( trace_t *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->DidHitWorld();
}

MONOAPI bool MONOFUNC(trace_did_hit_non_world_entity)( trace_t *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->DidHitNonWorldEntity();
}

MONOAPI int MONOFUNC(trace_get_entity_index)( trace_t *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->GetEntityIndex();
}

MONOAPI CBaseEntity *MONOFUNC(trace_hit_entity)( trace_t *pObject )
{
	if( pObject == NULL )
		return NULL;

	return pObject->m_pEnt;
}

MONOAPI float MONOFUNC(trace_fraction)( trace_t *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->fraction;
}

MONOAPI float MONOFUNC(trace_fraction_left_solid)( trace_t *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->fractionleftsolid;
}

MONOAPI bool MONOFUNC(trace_all_solid)( trace_t *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->allsolid;
}

MONOAPI bool MONOFUNC(trace_start_solid)( trace_t *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->startsolid;
}

MONOAPI Vector *MONOFUNC(trace_start_position)( trace_t *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->startpos );
}

MONOAPI Vector *MONOFUNC(trace_end_position)( trace_t *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->endpos );
}

MONOAPI Vector *MONOFUNC(trace_hit_normal)( trace_t *pObject )
{
	if( pObject == NULL || pObject->allsolid == true )
		return NULL;

	return new Vector( pObject->plane.normal );
}