//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: mathlib 3d vector binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MONOAPI Vector *MONOFUNC(vector_create_class)( float x, float y, float z )
{
	return new Vector( x, y, z );
}

MONOAPI void MONOFUNC(vector_destroy_class)( Vector *pObject )
{
	if( pObject == NULL )
		return;

	delete pObject;
	pObject = NULL;
}

MONOAPI void MONOFUNC(vector_set_vector)( Vector *pObject, float x, float y, float z )
{
	if( pObject == NULL )
		return;

	pObject->x = x;
	pObject->y = y;
	pObject->z = z;
}

MONOAPI float MONOFUNC(vector_get_x)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->x;
}

MONOAPI float MONOFUNC(vector_get_y)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->y;
}

MONOAPI float MONOFUNC(vector_get_z)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->z;
}

MONOAPI void MONOFUNC(vector_set_x)( Vector *pObject, float val )
{
	if( pObject == NULL )
		return;

	pObject->x = val;
}

MONOAPI void MONOFUNC(vector_set_y)( Vector *pObject, float val )
{
	if( pObject == NULL )
		return;

	pObject->y = val;
}

MONOAPI void MONOFUNC(vector_set_z)( Vector *pObject, float val )
{
	if( pObject == NULL )
		return;

	pObject->z = val;
}

MONOAPI float MONOFUNC(vector_length)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->Length();
}

MONOAPI float MONOFUNC(vector_lengthsqr)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->LengthSqr();
}

MONOAPI float MONOFUNC(vector_length2d)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->Length2D();
}

MONOAPI float MONOFUNC(vector_length2dsqr)( Vector *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->Length2DSqr();
}

MONOAPI float MONOFUNC(vector_dotproduct)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return 0;

	return pObject->Dot( *pOther );
}

MONOAPI Vector *MONOFUNC(vector_crosspoduct)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return 0;

	return new Vector( pObject->Cross( *pOther ) );
}

MONOAPI float MONOFUNC(vector_distto)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return 0;

	return pObject->DistTo( *pOther );
}

MONOAPI float MONOFUNC(vector_disttosqr)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return 0;

	return pObject->DistToSqr( *pOther );
}

MONOAPI bool MONOFUNC(vector_withinaabox)( Vector *pObject, Vector *pOtherA, Vector *pOtherB )
{
	if( pObject == NULL || pOtherA == NULL || pOtherB == NULL )
		return false;

	return pObject->WithinAABox( *pOtherA, *pOtherB );
}

MONOAPI Vector *MONOFUNC(vector_normalized)( Vector *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( pObject->Normalized() );
}

MONOAPI Vector *MONOFUNC(vector_add)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return NULL;

	return new Vector( *pObject + *pOther );
}

MONOAPI Vector *MONOFUNC(vector_sub)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return NULL;

	return new Vector( *pObject - *pOther );
}

MONOAPI Vector *MONOFUNC(vector_mul)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return NULL;

	return new Vector( *pObject * *pOther );
}

MONOAPI Vector *MONOFUNC(vector_div)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return NULL;

	return new Vector( *pObject / *pOther );
}

MONOAPI Vector *MONOFUNC(vector_mul_float)( Vector *pObject, float other )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( *pObject * other );
}

MONOAPI Vector *MONOFUNC(vector_div_float)( Vector *pObject, float other )
{
	if( pObject == NULL )
		return NULL;

	return new Vector( *pObject / other );
}

MONOAPI bool MONOFUNC(vector_equal)( Vector *pObject, Vector *pOther )
{
	if( pObject == NULL || pOther == NULL )
		return NULL;

	return *pObject == *pOther;
}