//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Partial filesystem binding for P/Invoke and init code
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"
#include "filesystem.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MONOAPI void MONOFUNC(filesystem_add_search_path)( char *path, char *pathID )
{
	filesystem->AddSearchPath( path, pathID );
}

MONOAPI void MONOFUNC(filesystem_remove_search_path)( char *path, char *pathID )
{
	filesystem->RemoveSearchPath( path, pathID );
}