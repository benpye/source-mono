//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Color binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MONOAPI Color *MONOFUNC(color_create_class)( int r, int g, int b, int a )
{
	return new Color( r, g, b, a );
}

MONOAPI void MONOFUNC(color_destroy_class)( Color *pObject )
{
	if( pObject == NULL )
		return;

	delete pObject;
	pObject = NULL;
}

MONOAPI void MONOFUNC(color_set_color)( Color *pObject, int r, int g, int b, int a )
{
	if( pObject == NULL )
		return;

	pObject->SetColor( r, g, b, a );
}

MONOAPI int MONOFUNC(color_get_r)( Color *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->r();
}

MONOAPI int MONOFUNC(color_get_g)( Color *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->g();
}

MONOAPI int MONOFUNC(color_get_b)( Color *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->b();
}

MONOAPI int MONOFUNC(color_get_a)( Color *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->a();
}