//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Tier1 convar/concmd binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

typedef struct
{
	const char **strings;
	int count;
} ArgStruct_t;

typedef void (__stdcall *concmd_callback_t)(ArgStruct_t args);

typedef struct
{
	char *name;
	char *help;
	ConCommand *command;
	concmd_callback_t callback;
} ConCmd_t;

CUtlVector<ConCmd_t> ConsoleCommands;
//concmd_callback_t g_pConcmdManagedCallback;

static void concmd_stub( const CCommand &args )
{
	ArgStruct_t argsToPass;
	argsToPass.strings = args.ArgV();
	argsToPass.count = args.ArgC();
	FOR_EACH_VEC( ConsoleCommands, i )
	{
		if( V_strcmp( args[0], ConsoleCommands[i].name ) == 0 )
		{
			ConsoleCommands[i].callback( argsToPass );
			return;
		}
	}
	//g_pConcmdManagedCallback(argsToPass);
}

/*MONOAPI void set_concmd_handler( concmd_callback_t callback )
{
	g_pConcmdManagedCallback = callback;
}*/

MONOAPI ConCommand *MONOFUNC(add_concmd)( char *command, char *help, int flags, concmd_callback_t callback )
{
	FOR_EACH_VEC( ConsoleCommands, i )
	{
		if( V_strcmp( ConsoleCommands[i].name, command ) == 0 )
		{
			DevWarning( "Console command %s already exists.\n", command );
			return NULL;
		}
	}

	ConCmd_t cmd;
	cmd.name = new char[V_strlen( command ) + 1];
	cmd.help = new char[V_strlen( help ) + 1];
	cmd.callback = callback;

	V_strcpy( cmd.name, command );
	V_strcpy( cmd.help, help );

	cmd.command = new ConCommand( cmd.name, concmd_stub, cmd.help, flags );

	ConsoleCommands.AddToHead( cmd );

	return cmd.command;
}

MONOAPI void MONOFUNC(remove_concmd)( char *command )
{
	int index = -1;

	FOR_EACH_VEC( ConsoleCommands, i )
	{
		if( V_strcmp( ConsoleCommands[i].name, command ) == 0 )
		{
			index = i;
			break;
		}
	}

	if( index == -1 )
	{
		DevWarning( "Attempting to remove non existant console command %s.\n", command );
		return;
	}

	if( !ConsoleCommands.IsValidIndex( index ) )
	{
		DevWarning( "Attempting to remove console command failed!\n" );
		return;
	}

	cvar->UnregisterConCommand( ConsoleCommands[index].command );
	delete ConsoleCommands[index].command;
	delete [] ConsoleCommands[index].name;
	delete [] ConsoleCommands[index].help;
	ConsoleCommands.Remove( index );
}

MONOAPI bool MONOFUNC(concommandbase_is_command)( ConCommandBase *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsCommand();
}

MONOAPI void MONOFUNC(concommandbase_add_flags)( ConCommandBase *pObject, int flag )
{
	if( pObject == NULL )
		return;

	return pObject->AddFlags( flag );
}

MONOAPI void MONOFUNC(concommandbase_remove_flags)( ConCommandBase *pObject, int flag )
{
	if( pObject == NULL )
		return;

	return pObject->RemoveFlags( flag );
}

MONOAPI int MONOFUNC(concommandbase_get_flags)( ConCommandBase *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetFlags();
}

MONOAPI char *MONOFUNC(concommandbase_get_name)( ConCommandBase *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetName();
}

MONOAPI char *MONOFUNC(concommandbase_get_help)( ConCommandBase *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetHelpText();
}

typedef struct
{
	char *name;
	char *help;
	char *defaultValue;
	int flags;
	ConVar *cvar;
} ConVar_t;

CUtlVector<ConVar_t> ConsoleVariables;

MONOAPI ConVar *MONOFUNC(convar_create_class)( char *name, char *defaultValue, int flags, char *help )
{
	ConVar_t convar;
	convar.name = new char[ V_strlen( name ) + 1 ];
	convar.defaultValue = new char[ V_strlen( defaultValue ) + 1 ];
	convar.help = new char[ V_strlen( help ) + 1 ];
	convar.flags = flags;

	V_strcpy( convar.name, name );
	V_strcpy( convar.defaultValue, defaultValue );
	V_strcpy( convar.help, help );

	ConVar *newCvar = new ConVar( convar.name, convar.defaultValue, convar.flags, convar.help );

	convar.cvar = newCvar;

	ConsoleVariables.AddToHead( convar );

	return newCvar;
}

MONOAPI void MONOFUNC(convar_destroy_class)( ConVar *pObject )
{
	if( pObject == NULL )
		return;

	FOR_EACH_VEC( ConsoleVariables, i )
	{
		if( ConsoleVariables[i].cvar == pObject )
		{
			cvar->UnregisterConCommand( pObject );
			delete [] ConsoleVariables[i].name;
			delete [] ConsoleVariables[i].help;
			delete [] ConsoleVariables[i].defaultValue;

			ConsoleVariables.Remove( i );
			break;
		}
	}

	delete pObject;
}

MONOAPI float MONOFUNC(convar_get_float)( ConVar *pObject )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetFloat();
}

MONOAPI int MONOFUNC(convar_get_int)( ConVar *pObject )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetInt();
}

MONOAPI Color *MONOFUNC(convar_get_color)( ConVar *pObject )
{
	if( pObject == NULL )
		return NULL;

	return new Color( pObject->GetColor() );
}

MONOAPI bool MONOFUNC(convar_get_bool)( ConVar *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->GetBool();
}

MONOAPI char *MONOFUNC(convar_get_string)( ConVar *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetString();
}

MONOAPI void MONOFUNC(convar_set_string)( ConVar *pObject, char *value )
{
	if( pObject == NULL )
		return;

	pObject->SetValue( value );
}

MONOAPI void MONOFUNC(convar_set_float)( ConVar *pObject, float value )
{
	if( pObject == NULL )
		return;

	pObject->SetValue( value );
}

MONOAPI void MONOFUNC(convar_set_int)( ConVar *pObject, int value )
{
	if( pObject == NULL )
		return;

	pObject->SetValue( value );
}

MONOAPI void MONOFUNC(convar_set_color)( ConVar *pObject, Color *value )
{
	if( pObject == NULL || value == NULL )
		return;

	pObject->SetValue( *value );
}

MONOAPI char *MONOFUNC(convar_get_default)( ConVar *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetDefault();
}