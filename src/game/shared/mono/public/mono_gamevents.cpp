//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Game events binding for P/Invoke and init code
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"
#include "filesystem.h"
#include "igameevents.h"
#include "GameEventListener.h"

#include "mono_gameevents.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

typedef void (__stdcall *game_event_callback_t)(IGameEvent *event_pointer);

game_event_callback_t g_pGameEventCallback;
game_event_callback_t g_pGameEventCallbackServer;

class MonoEvent : public CGameEventListener
{
	virtual void FireGameEvent( IGameEvent *event )
	{
		g_pGameEventCallback( event );
	}
};

MonoEvent *g_pGameEventListener;

void mono_gameevents_addfile( const char * filename )
{
	KeyValues *key = new KeyValues( filename );
	KeyValues::AutoDelete autodelete_key( key );

	if( !key->LoadFromFile( (IBaseFileSystem *)filesystem, filename, "GAME" ) )
		return;

	KeyValues *subkey = key->GetFirstSubKey();

	while ( subkey )
	{
		if ( subkey->GetDataType() == KeyValues::TYPE_NONE )
		{
			g_pGameEventListener->ListenForGameEvent( subkey->GetName() );
		}

		subkey = subkey->GetNextKey();
	}
}

void mono_gameevents_init()
{
	g_pGameEventListener = new MonoEvent();
	// Load the default files, add them to our listener

#ifndef GAME_DLL
	mono_gameevents_addfile( "resource/ModEvents.res" );
#endif
	mono_gameevents_addfile( "resource/serverevents.res" );
	mono_gameevents_addfile( "resource/gameevents.res" );
}

MONOAPI void MONOFUNC(set_game_event_callback)( game_event_callback_t callback )
{
	DevMsg("set_game_event_callback\n");
	g_pGameEventCallback = callback;
}

MONOAPI char * MONOFUNC(gameevent_get_name)( IGameEvent *pObject )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetName();
}

MONOAPI bool MONOFUNC(gameevent_is_reliable)( IGameEvent *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsReliable();
}

MONOAPI bool MONOFUNC(gameevent_is_local)( IGameEvent *pObject )
{
	if( pObject == NULL )
		return false;

	return pObject->IsLocal();
}

MONOAPI bool MONOFUNC(gameevent_is_empty)( IGameEvent *pObject, char *keyName )
{
	if( pObject == NULL )
		return false;

	return pObject->IsEmpty( keyName );
}

MONOAPI bool MONOFUNC(gameevent_get_bool)( IGameEvent *pObject, char *keyName, bool defaultValue )
{
	if( pObject == NULL )
		return false;

	return pObject->GetBool( keyName, defaultValue );
}

MONOAPI int MONOFUNC(gameevent_get_int)( IGameEvent *pObject, char *keyName, int defaultValue )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetInt( keyName, defaultValue );
}

MONOAPI uint64 MONOFUNC(gameevent_get_uint64)( IGameEvent *pObject, char *keyName, uint64 defaultValue )
{
	if( pObject == NULL )
		return 0;

	return pObject->GetUint64( keyName, defaultValue );
}

MONOAPI float MONOFUNC(gameevent_get_float)( IGameEvent *pObject, char *keyName, float defaultValue )
{
	if( pObject == NULL )
		return 0.0f;

	return pObject->GetFloat( keyName, defaultValue );
}

MONOAPI char *MONOFUNC(gameevent_get_string)( IGameEvent *pObject, char *keyName, const char *defaultValue )
{
	if( pObject == NULL )
		return "";

	return (char *)pObject->GetString( keyName, defaultValue );
}

MONOAPI void MONOFUNC(gameevent_set_bool)( IGameEvent *pObject, char *keyName, bool val )
{
	if( pObject == NULL )
		return;

	pObject->SetBool( keyName, val );
}

MONOAPI void MONOFUNC(gameevent_set_int)( IGameEvent *pObject, char *keyName, int val )
{
	if( pObject == NULL )
		return;

	pObject->SetInt( keyName, val );
}

MONOAPI void MONOFUNC(gameevent_set_uint64)( IGameEvent *pObject, char *keyName, uint64 val )
{
	if( pObject == NULL )
		return;

	pObject->SetUint64( keyName, val );
}

MONOAPI void MONOFUNC(gameevent_set_float)( IGameEvent *pObject, char *keyName, float val )
{
	if( pObject == NULL )
		return;

	pObject->SetFloat( keyName, val );
}

MONOAPI void MONOFUNC(gameevent_set_string)( IGameEvent *pObject, char *keyName, const char *val )
{
	if( pObject == NULL )
		return;

	pObject->SetString( keyName, val );
}

MONOAPI IGameEvent *MONOFUNC(gameevent_create_event)( char *name )
{
	return gameeventmanager->CreateEvent( name, true );
}

MONOAPI void MONOFUNC(gameevent_destroy_event)( IGameEvent *pObject )
{
	if( pObject == NULL )
		return;

	gameeventmanager->FreeEvent( pObject );
}

MONOAPI IGameEvent *MONOFUNC(gameevent_duplicate_event)( IGameEvent *pObject )
{
	if( pObject == NULL )
		return NULL;

	return gameeventmanager->DuplicateEvent( pObject );
}

MONOAPI void MONOFUNC(gameevent_fire)( IGameEvent *pObject, bool serveronly )
{
	if( pObject == NULL )
		return;

	gameeventmanager->FireEvent( pObject, serveronly );
}

MONOAPI void MONOFUNC(gameevent_fire_clientside)( IGameEvent *pObject )
{
	if( pObject == NULL )
		return;

	gameeventmanager->FireEventClientSide( pObject );
}