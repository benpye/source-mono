//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose: Tier0 dbg binding for P/Invoke
//
//===========================================================================//

#include "cbase.h"

#include "mono/monomanager.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

MONOAPI void MONOFUNC(dbg_msg)( char *fmt )
{
	Msg( fmt );
}

MONOAPI void MONOFUNC(dbg_warning)( char *fmt )
{
	Warning( fmt );
}

MONOAPI void MONOFUNC(dbg_error)( char *fmt )
{
	Error( fmt );
}

MONOAPI void MONOFUNC(dbg_devmsg)( char *fmt )
{
	DevMsg( fmt );
}

MONOAPI void MONOFUNC(dbg_devwarning)( char *fmt )
{
	DevWarning( fmt );
}

MONOAPI void MONOFUNC(dbg_colormsg)( Color *pColor, char *fmt )
{
	if( pColor == NULL )
		return;

	ConColorMsg( (const Color)*pColor, fmt );
}