//======= Copyright � 2012 - 2013, Source Mono, All rights reserved. ========//
//
// Purpose:
//
//===========================================================================//

#define TOKENPASTE(x, y) x ## y
#define TOKENPASTE2(x, y) TOKENPASTE(x, y)
#define MONOFUNC(x) TOKENPASTE2(DLLNAME,_##x)

#ifndef MONO_MANAGER_H
#define MONO_MANAGER_H

#ifdef _WIN32
#define MONOAPI extern "C" _declspec(dllexport)
#else
#define MONOAPI extern "C"
#endif

void mono_init();
void mono_quit();

#endif