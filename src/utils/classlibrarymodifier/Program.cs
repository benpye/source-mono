﻿using System;
using System.Linq;
using System.IO;
using Mono.Cecil;
using Mono.Cecil.Mdb;

namespace classlibrarymodifier
{
	class Program
	{
		private static void ProcessDirectory(string libDir, string target)
		{
			var di = new DirectoryInfo(libDir);

			var targetPath = di.Parent.FullName;
			targetPath += "/" + target;

			var entryPointPrefix = target + "_";

			var internalRef = new ModuleReference("__Internal");

			// We need to do this as it's looking for the assembly when use an enum constant
			// So lets tell it where the assemblies are coming from so it can load them
			var assemblyResolve = new DefaultAssemblyResolver();
			assemblyResolve.AddSearchDirectory(libDir);

			var readParams = new ReaderParameters { AssemblyResolver = assemblyResolve };

			var writeParams = new WriterParameters { SymbolWriterProvider = new MdbWriterProvider(), WriteSymbols = true };

			foreach (var file in di.EnumerateFiles("*.dll").Union(di.EnumerateFiles("*.exe")))
			{
				ModuleDefinition md;

				// Only load the pdb if it exists, stops issues with Nuget packages, eg Newtonsoft.Json
				if (new FileInfo(file.FullName.Replace(file.Extension, "pdb")).Exists)
					readParams.ReadSymbols = true;
				else
					readParams.ReadSymbols = false;
				
				md = ModuleDefinition.ReadModule(file.FullName, readParams);

				md.ModuleReferences.Add(internalRef);
				Console.WriteLine("Type count: {0}", md.Types.Count);
				// Process P/Invokes
				foreach (var mdType in md.Types)
				{
					Console.WriteLine("Method count: {0}", mdType.Methods.Count);
					foreach (var method in mdType.Methods)
					{
						if (method.IsPInvokeImpl)
						{
							if (method.PInvokeInfo.Module.Name == "__Source")
							{
								Console.WriteLine("Fixing entry point for: {0}", method.FullName);
								Console.WriteLine("From: {0}", method.PInvokeInfo.EntryPoint);
								method.PInvokeInfo.EntryPoint = entryPointPrefix + method.PInvokeInfo.EntryPoint;
								Console.WriteLine("To: {0}", method.PInvokeInfo.EntryPoint);

								method.PInvokeInfo.Module = internalRef;
							}
						}
					}
				}

				md.Write(targetPath + "/" + file.Name, writeParams);
			}
		}

		static void Main(string[] args)
		{
			// <in> <target>
			if (args.Length < 2)
			{
				Console.WriteLine("Usage: classlibrarymodifier <input directory> <target>");
			}

			var inputPath = args[0];
			var target = args[1];

			ProcessDirectory(inputPath, target);
		}
	}
}
