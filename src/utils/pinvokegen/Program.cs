﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace pinvokegen
{
	class Argument
	{
		public Argument(string name, string paramType)
		{
			Name = name;
			ParamType = paramType;
		}

		public string Name;
		public string ParamType;
	}

	class Function
	{
		public string ReturnType;
		public string FunctionName;
		public List<Argument> Arguments;
	}

	class Program
	{
		static void Main(string[] args)
		{
			// <in> <out>
			if (args.Length < 2)
			{
				Console.WriteLine("Usage: pinvokegen <input file> <output file>");
			}

			var inFile = File.OpenText(args[0]);
			var outFile = File.Open(args[1], FileMode.OpenOrCreate);
			var outWrite = new StreamWriter(outFile);

			var functions = new List<Function>();

			while (inFile.Peek() >= 0)
			{
				var line = inFile.ReadLine();
				line = line.Trim();
				if (line.Length < 8)
					continue;
				if (line.Substring(0, 8) == "MONOAPI ")
				{
					line = line.Substring(8);

					var func = new Function();

					var splitLine = line.Split(new[] { "MONOFUNC(" }, StringSplitOptions.None);
					var returnType = splitLine[0].Trim();

					if (splitLine[0][splitLine[0].Length-1] == '*')
					{
						returnType = String.Format("/*{0}*/ IntPtr", returnType);
					}

					func.ReturnType = returnType;

					splitLine = splitLine[1].Split(new[] { ")(" }, StringSplitOptions.None);
					var functionName = splitLine[0];
					func.FunctionName = functionName;

					var funcArgs = new List<string>();
					var rawParams = splitLine[1].Substring(0, splitLine[1].Length - 2);

					func.Arguments = new List<Argument>();

					var paramArray = rawParams.Split(',');
					foreach (var rawParam in paramArray)
					{
						var ptrCount = rawParam.Count(c => c == '*');
						var param = rawParam.Replace("*", "");
						var rawSplitParam = param.Split(' ');
						var splitParam = new string[2];
						splitParam[1] = rawSplitParam[rawSplitParam.Length - 1];
						var addString = "";
						for (var i = 0; i < rawSplitParam.Length - 1; i++)
						{
							addString += rawSplitParam[i] + " ";
						}
						splitParam[0] = addString.Trim();
						var paramType = splitParam[0].Trim();
						if (ptrCount > 0)
						{
							if (paramType == "char" && ptrCount == 1)
							{
								paramType = "string";
							}
							else
							{
								var ptrLevel = new String('*', ptrCount);
								paramType = String.Format("/*{0} {1}*/ IntPtr", paramType, ptrLevel);
							}
						}

						if (paramType == "unsigned int")
						{
							paramType = "uint";
						}

						var paramName = splitParam[1].Trim();

						func.Arguments.Add(new Argument(paramName, paramType));

						funcArgs.Add(String.Format("{0} {1}", paramType, paramName));
					}

					outWrite.WriteLine("[DllImport(\"__Source\")]");
					outWrite.Write("private static extern ");
					outWrite.Write(returnType);
					outWrite.Write(" ");
					outWrite.Write(functionName);
					outWrite.Write("(");
					var paramString = "";
					foreach (var arg in funcArgs)
					{
						paramString += arg;
						paramString += ", ";
					}

					paramString = paramString.Substring(0, paramString.Length - 2);
					outWrite.Write(paramString);
					outWrite.WriteLine(");");
					outWrite.WriteLine("");
					functions.Add(func);
				}
			}

			outWrite.WriteLine("");

			foreach (var func in functions)
			{
				var nameParts = func.FunctionName.Split('_').ToList();
				nameParts.RemoveAt(0);

				var formattedName = "";

				foreach (var part in nameParts)
				{
					formattedName += part[0].ToString().ToUpper() + part.Substring(1).ToLower();
				}

				outWrite.Write(String.Format("public {0} {1}(", func.ReturnType, formattedName));

				var inputArgs = func.Arguments;

				var isClassFunction = false;

				if (inputArgs[0].Name == "pObject")
				{
					isClassFunction = true;
					inputArgs.RemoveAt(0);
				}

				if (inputArgs.Any())
				{
					var argString = "";
					foreach (var arg in inputArgs)
					{
						argString += String.Format("{0} {1}, ", arg.ParamType, arg.Name);
					}

					argString = argString.Substring(0, argString.Length - 2);

					outWrite.Write(argString);
				}

				outWrite.WriteLine(")");
				outWrite.WriteLine("{");
				outWrite.Write("\t");
				if (func.ReturnType != "void")
				{
					outWrite.Write("return ");
				}
				outWrite.Write(func.FunctionName);

				var funcArgs = "(";
				if (isClassFunction)
					funcArgs += "NativePtr, ";

				if (inputArgs.Any())
				{
					foreach (var arg in inputArgs)
					{
						funcArgs += String.Format("{0}, ", arg.Name);
					}
				}

				funcArgs = funcArgs.Substring(0, funcArgs.Length - 2);
				outWrite.Write(funcArgs);
				outWrite.WriteLine(");");
				outWrite.WriteLine("}");
			}

			outWrite.Close();
			outFile.Close();
			inFile.Close();
		}
	}
}
